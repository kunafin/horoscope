
import React, { useState } from 'react';
import { Platform, StatusBar, StyleSheet, View ,Text, NetInfo,} from 'react-native';
import * as Permissions from 'expo-permissions';
import { ActionSheetProvider } from '@expo/react-native-action-sheet'
import { Provider } from 'react-redux';
import store from './src/store';

import AppNavigator from './src/navigation/AppNavigator';


class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = { 
      connect: null,
      refresh: false
    };
  }
  
  componentDidMount() {
    //запрос на получение уведомлений
    Permissions.askAsync(Permissions.NOTIFICATIONS);

    NetInfo.getConnectionInfo().then((connectionInfo) => {
      console.log(
         'Initial, type: ' + 
          connectionInfo.type + 
         ', effectiveType: ' + 
          connectionInfo.effectiveType);
    });
    NetInfo.addEventListener(
      'connectionChange',
      (connectionInfo) => this.handleFirstConnectivityChange(connectionInfo)
    )
  }
  
  handleFirstConnectivityChange(connectionInfo) {
    this.setState({connect: connectionInfo.type})
    console.log('First change, type: ' + 
                connectionInfo.type + 
                ', effectiveType: ' + 
                connectionInfo.effectiveType);
  }
  render() {
    let {locale} = this.state
    console.log(locale)
    return (
      <View style={styles.container}>
        <StatusBar backgroundColor="blue" barStyle="light-content" />

        <AppNavigator locale={locale}/>

        {this.state.connect == "none" 
          ? <View style={styles.containerNoConnect}>
              <Text style={{textAlign: "center", color: "white"}}>Нет доступа к сети. {"\n"}Без интернета звезды молчат...</Text>
            </View>
          : null
        }
      </View>
    );
  }
  
}

export default class AppContainer extends React.Component {
  render () {
    return (
      <ActionSheetProvider>
         <Provider store={store}>
          <App />
        </Provider>
      </ActionSheetProvider>
    );
  }
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  containerNoConnect: {
    flex: 1, 
    paddingBottom: 30,
    paddingTop: 10,
    bottom: 0, 
    backgroundColor: "red",
    width: "100%", 
    position: "absolute", 
    alignItems: "center", 
    justifyContent: "center",
    opacity: 0.8
  }
});
