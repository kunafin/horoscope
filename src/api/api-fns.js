import {
  AsyncStorage,
} from 'react-native';
import config from '../../app.json'
const HOST = config.host
const HEADER = {
  'Accept': 'application/json, text/plain, */*',
  'Content-Type': 'application/json',
  'Accept-Language': 'ru'
}

import Token from './token';

const api = {
  getLocale() {
    return AsyncStorage.getItem('@UserLocale:key')
            .then((response) => {
                if(response !== null) {
                    return response;
                } else {
                    return false
                }
            });
  },
  async setLocale(locale) {
    try {
        await AsyncStorage.setItem('@UserLocale:key', locale);
        console.log('SET LOCALE', locale);
        return locale;
    } catch (error) {
        console.log(error);
    }
  },
  getSettingCalendar() {
    return Token.getToken().then((token_hash)=> {
      return this.getLocale().then((lang) => {
        HEADER['Authorization'] = token_hash;
        HEADER['Accept-Language'] = lang;
        return fetch(`${HOST}/horoscopes/calendar`, {
          method: 'get',
          headers: HEADER,
        }).then(res=>res.json())
      })
    })
  },
  getUser() {
    return Token.getToken().then((token_hash)=> {
      HEADER['Authorization'] = token_hash;
      return fetch(`${HOST}/me`, {
          method: 'get',
          headers: HEADER,
        }).then(res=>res.json())
    })
  },
  createOrUpdateUser(user) {
    return Token.getToken().then((token_hash)=> {
      if(token_hash) {
        HEADER['Authorization'] = token_hash;
        return fetch(`${HOST}/me`, {
            method: 'patch',
            headers: HEADER,
            body: JSON.stringify(user)
          }).then(res=>res.json())
      } else {
        return fetch(`${HOST}/users`, {
          method: 'post',
          headers: HEADER,
          body: JSON.stringify(user)
        }).then(res=> res.json())
      }
    })
    
  },
  getHoroscope(period, date, zodiak_sign) {
    console.log('api getHoroscope(period, date, zodiak_sign)', period, date, zodiak_sign )
    return Token.getToken().then((token_hash)=> {
     
      return this.getLocale().then((lang) => {
        HEADER['Authorization'] = token_hash;
        HEADER['Accept-Language'] = lang;
        return fetch(`${HOST}/horoscopes?period=${period}&date=${date}&zodiak_sign=${zodiak_sign}`, {
            method: 'get',
            headers: HEADER,
          }).then(res=>res.json())
      })
    })
  },
  getRate(horoscope_id) {
    return Token.getToken().then((token_hash)=> {
      HEADER['Authorization'] = token_hash;
      return fetch(`${HOST}/horoscopes/${horoscope_id}/rate`, {
          method: 'get',
          headers: HEADER,
        }).then(res=>res.json())
    })
  },
  setRate(horoscope_id, rate) {
    return Token.getToken().then((token_hash)=> {
      HEADER['Authorization'] = token_hash;
      return fetch(`${HOST}/horoscopes/${horoscope_id}/rate`, {
          method: 'post',
          headers: HEADER,
          body: JSON.stringify({
            "precision": rate
          })
        }).then(res=>res.json())
    })
  },
  getCompatibility(zodiak_my, zodiak_partner) {
    console.log(zodiak_my, zodiak_partner)
    return Token.getToken().then((token_hash)=> {
      return this.getLocale().then((lang) => {
        HEADER['Authorization'] = token_hash;
        HEADER['Accept-Language'] = lang;
        return fetch(`${HOST}/compatibility/?zodiak_sign1=${zodiak_my}&zodiak_sign2=${zodiak_partner}`, {
          method: 'get',
          headers: HEADER,
        }).then(res=>res.json())
      })
      
    })
  },
  sendFeedback(email, text) {
    return Token.getToken().then((token_hash)=> {
      HEADER['Authorization'] = token_hash;
      return fetch(`${HOST}/feedback`, {
          method: 'post',
          headers: HEADER,
          body: JSON.stringify({
            "email": email,
            "text" : text
          })
        }).then(res=>res.status)
    })
  }
};

module.exports = api;