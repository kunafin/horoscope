
import Locale from '../constants/Locale';

import i18n from '../i18n';

const helper = {
    ObjToArr(data) {
        if(typeof(data) == 'object') {
            return Object.keys(data).map(key => ({ key, value: data[key] }));
        }
        return false;
    },
    inArray(value, array) {
      if(array.indexOf(value) != -1)
      {  
            return true
      }
      return false
    },
    validateEmail(email) {
      var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      return re.test(String(email).toLowerCase());
    },
    parserDate(date){
      var d = new Date(date),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = '' +d.getFullYear();
    
        if (month.length < 2) month = '0' + month;
        if (day.length < 2) day = '0' + day;

        return {
          day,
          month,
          year
        }
    },
    formatNumber(number) {
        return  String(number).replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1 ");
    },
    formatDate(date, type = "yyyy-mm-dd") {
        let {day, month, year} = this.parserDate(date);
        switch (type) {
          case "yyyy-mm-dd":
            return [year, month, day].join('-');
        
          case "dd.mm.yyyy":
            return [day, month, year].join('.');
        }
       
    },
    getWeek(d) {
        // Copy date so don't modify original
        d = new Date(Date.UTC(d.getFullYear(), d.getMonth(), d.getDate()));

        // Set to nearest Thursday: current date + 4 - current day number
        // Make Sunday's day number 7
        d.setUTCDate(d.getUTCDate() + 4 - (d.getUTCDay()||7));
        
        //Set the start date of the week, Monday
        let startWeekDay =  new Date(d.getTime() - 86400 * 1000 * 3);
        //Set the end date of the week, Sunday
        let endWeekDay = new Date(d.getTime() + 86400 * 1000 * 3);
        // Get first day of year
        let yearStart = new Date(Date.UTC(d.getUTCFullYear(),0,1));
        // Calculate full weeks to nearest Thursday
        let weekNo = Math.ceil(( ( (d - yearStart) / 86400000) + 1)/7);

        
        // Return array of year and week number
        return {
            'year' : d.getUTCFullYear(), 
            'weekNo' : weekNo,
            'startWeekDay' : startWeekDay,
            'endWeekDay' : endWeekDay,
        };
    },
    getDate(d, locale = "ru") {
      let date = new Date(d)
      return date.getDate() + " " + Locale[ locale ]["monthRodNames"][date.getMonth()] 
    },
    getDayWeek(d, locale = "ru") {
      let date = new Date(d)
      return Locale[ locale ]["dayNames"][date.getDay()]
    },
    getFullDate(d, locale) {
      return this.getDate(d, locale) + " " + this.getYear(d, locale)
    },
    getWeekStr(d, locale = "ru") {
      let week = this.getWeek(new Date(d));
      return  this.getFullDate(week.startWeekDay, locale)+ " — " + this.getFullDate(week.endWeekDay, locale)
    },
    getWeekNoStr(d, locale ="ru") {
      let week = this.getWeek(new Date(d));
      return Locale[ locale ].week + " " + week.weekNo
    },
    getMonth(d, locale = "ru") {
      let date = new Date(d)
      return Locale[ locale ]["monthNames"][date.getMonth()]
    },
    getYear(d) {
      let date = new Date(d);
      return date.getFullYear()
    },
    getTopTitleDate(item, date, locale = "ru") {
      if(this.inArray(item.name, ["tomorrow", "today", "day_after_tomorrow", "yesterday", "day_before_yesterday"])) {
        //среда, 7 августа
        return this.getDayWeek(date, locale) + ", " + this.getDate(date, locale)
      } 
      
      else if (item.type == "day") {
        //среда
        return this.getDayWeek(date, locale)
      } 
      else if (item.type == "week") {
        // 12 авгста 2019 - 18 августа 2019
        return this.getWeekStr(date, locale)
      } 
      else if (item.type == "month") {
        //2019
        return this.getYear(date)
      }
    },
    getTitleDate(item, date, locale = "ru") {
      if(item.name == "today") {
        //Сегодня
        return Locale[ locale ].today
      } 
      else if(item.name == "tomorrow") {
        //Завтра
        return Locale[ locale ].tomorrow
      } 
      else if(item.name == "day_after_tomorrow") {
        //Послезавтра
        return Locale[ locale ].day_after_tomorrow
      }
      else if(item.name == "yesterday") {
        //Вчера
        return Locale[ locale ].yesterday
      }  
      else if(item.name == "day_before_yesterday") {
        //Позавчера
        return Locale[ locale ].day_before_yesterday
      } 
      else if (item.type == "day") {
        //30 февраля
        return this.getDate(date, locale)
      } 
      else if (item.type == "month") {
        //Март
        return this.getMonth(date, locale)
      } 
      else if (item.type == "week") {
        //Неделя 33
        return this.getWeekNoStr(date, locale)
      } 
      else if (item.type == "year") {
        //2019
        return this.getYear(date)
      } 
      else {
        return item.name
      }
    },
    isIncludedRangeDate (date, minDate, maxDate) {
      date = this.formatDate(date, "yyyy-mm-dd")
      return new Date(minDate) <= new Date(date) && new Date(date) <= new Date(maxDate)
    },
    datePastFuturePresent(d = Date.now()) {
        //dates for swipe slider
        d = new Date(d)
        const day_second = 86400 * 1000;
        
        return [
          {
            name: "year",
            date: new Date(Date.UTC(d.getFullYear(), 0, 1)),
            type: "year"
          },
          {
            name: "month",
            date: new Date(Date.UTC(d.getFullYear(), d.getMonth(), 1)),
            type: "month"
          },
          {
            name: "week",
            date: helper.getWeek(d).startWeekDay,
            type: "week"
          },
          {
            name: "day_before_yesterday",
            date: new Date(d.getTime() - day_second * 2),
            type: "day"
          },
          {
            name: "yesterday",
            date: new Date(d.getTime() - day_second * 1),
            type: "day"
          },
          {
            name: "today",
            date: d,
            type: "day"
          },
          {
            name: "tomorrow",
            date: new Date(d.getTime() + day_second * 1),
            type: "day"
          },
          {
            name: "day_after_tomorrow",
            date: new Date(d.getTime() + day_second * 2),
            type: "day"
          },
          {
            name: "next_week",
            date: helper.getWeek(new Date(d.getTime() + day_second * 7)).startWeekDay,
            type: "week"
          },
          {
            name: "next_month",
            date: new Date(Date.UTC(d.getFullYear(), d.getMonth()+1, 1)),
            type: "month"
          },
          {
            name: "next_year",
            date: new Date(Date.UTC(d.getFullYear()+1, 0, 1)),
            type: "year"
          },
        ]
      }
  };
  
  module.exports = helper;