import React from 'react';
import {
  Image,
  StyleSheet,
  Text,
  TouchableOpacity,
  StatusBar,
  View,
} from 'react-native';
import i18n from '../../../i18n';
import { connect } from 'react-redux';
import {bindActionCreators} from 'redux';
import * as ActionsUser from '../../../actions/User';
import * as ActionsSetting from '../../../actions/Setting';

import IconMenu from '../../../components/IconMenu'
import TextZodiak from '../../../components/TextZodiak'
import Colors from '../../../constants/Colors'

import LanguageButtom from '../components/LanguageButtom'

class SettingScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = { 
      email: "",
      text: "",
    };
  }
  
  static navigationOptions = ({ navigation }) => {
    return {
      title: i18n.t("setting"),
      headerLeft: <IconMenu color="black" navigation={navigation} style={{paddingLeft: 20}}/>
    }
  };
  getUserName() {
    let {user} = this.props
    let username = `${user.name}, ${TextZodiak({zodiak : user.zodiak_sign})}`
    
    return username
  }
  render () {
    let {navigation, setting} = this.props
   
    return (
      <View style={[styles.container]}>
        <StatusBar barStyle="default" />
        <View style={styles.contentContainer}>

          <TouchableOpacity style={styles.setting_item} onPress={()=>navigation.navigate("Username")}>
              <View style={{flexDirection: "row"}}>
                <Image style={styles.img} source={require("../../../assets/images/profile_mini.png")} />
                <Text style={styles.text}>{i18n.t("profile")}</Text>
              </View>
              
              <Text 
                style={[styles.text, styles.text_right, {flex:1, paddingLeft: 5, color: Colors.grey}]} 
                numberOfLines={1} 
                ellipsizeMode="middle">
                {this.getUserName()}
              </Text>

          </TouchableOpacity>
        
          <LanguageButtom >
            <View  style={styles.setting_item}>
                <View style={{flexDirection: "row"}}>
                  <Image style={styles.img} source={require("../../../assets/images/flag.png")} />
                  <Text style={styles.text}>{i18n.t("language")}</Text>
                </View>
                
                <Text 
                  style={[styles.text, styles.text_right, {flex:1, paddingLeft: 5, color: Colors.grey}]} 
                  numberOfLines={1} 
                  ellipsizeMode="middle">
                  {i18n.t( setting.locale )}
                </Text>
              </View>
          </LanguageButtom>
        </View>    
    </View>
    )
  }
}



export default  connect(state => ({
  user: state.user, 
  setting: state.setting
}),
(dispatch) => ({
  actions: {
    user: bindActionCreators(ActionsUser, dispatch),
    setting: bindActionCreators(ActionsSetting, dispatch)
  }
})
)(SettingScreen);


const styles = StyleSheet.create({
  container: {
      flex: 1,
      
  },  
  img: {
    marginRight: 15
  },
  text_right: {
    textAlign: "right"
  },  
  text: {
    fontSize: 16
  },
  contentContainer: {
      alignContent: "center",
      alignItems: "center",
      flex: 1,
      paddingBottom: 60,
      paddingTop: 20
  },
  setting_item: {
    flexDirection: "row", 
    justifyContent: "space-between", 
    width: "100%",
    borderBottomWidth: 1,
    borderBottomColor: "#ececec",
    padding: "4%",
    paddingTop: 0,
    paddingBottom: 15,
    marginBottom: 20
  }
})