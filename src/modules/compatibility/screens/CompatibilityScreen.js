import React from 'react';
import {
  Platform,
  Text,
  ScrollView,
  StyleSheet,
  TouchableOpacity,
  ImageBackground,
  StatusBar,
  View,
} from 'react-native';

import i18n from '../../../i18n'

import Markdown from 'react-native-markdown-renderer';

import { connect } from 'react-redux';
import {bindActionCreators} from 'redux';
import * as ActionsUser from '../../../actions/User';
import * as ActionsCompatibility from '../../../actions/Compatibility';

import Colors from '../../../constants/Colors'

import Loading from '../../../components/Loading'
import IconShare from '../../../components/IconShare'

import TextZodiak from '../../../components/TextZodiak';

import ZodiakImages from '../../../constants/ZodiakImages';
import IconCompatibility from '../components/IconCompatibility';
import IconMenu from '../../../components/IconMenu';
import { Ionicons } from '@expo/vector-icons';

const ZodiakButton = ({zodiak, onPress}) => {
 
  return (
    <TouchableOpacity 
        onPress={()=>onPress()} 
        >
        <View style={styles.zodiak_title}>
          <Text style={styles.zodiak_title_text}>
            <TextZodiak zodiak={zodiak} />
          </Text>
          <Ionicons
            name={"ios-arrow-down"}
            size={17}
            style={{ paddingTop: 5, paddingLeft: 5  }}
            color={Colors.black}
          />
        </View>
      </TouchableOpacity>
  );
}


class CompatibilityScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = { 
      
    };
  }
  static navigationOptions = {
    header: null,
  };
  componentWillMount() {
    let {actions, user, compatibility, navigation} = this.props
    let {zodiak_my, zodiak_partner} = compatibility
    let zodiak = zodiak_my ? zodiak_my : user.zodiak_sign
    
    actions.compatibility.setZodiakMy(zodiak)
    actions.compatibility.getCompatibility(zodiak, zodiak_partner);

    navigation.navigate('ZodiakPartner')
  }
  _parseText(text) {
    if(typeof text == 'object') {
      return text
    } else {
      return JSON.parse(text)
    }
  }
  _shareTitle(){
    let {compatibility} = this.props
    let {rate, zodiak_sign1, zodiak_sign2} = compatibility.compatibility
    return `${i18n.t('compatibility')} ${TextZodiak({zodiak: zodiak_sign1})} ${i18n.t('and')} ${TextZodiak({zodiak: zodiak_sign2})} – ${rate}%`
  }
  _shareText() {
    let {compatibility} = this.props
    let {texts} = compatibility.compatibility
    return this._parseText(texts).map((text, index) => {
      return text
    })
  }
  render() {
    
    let {navigation, compatibility} = this.props

    if(!compatibility.compatibility) {
      return <Loading/>
    }
    let {rate, texts, zodiak_sign1, zodiak_sign2} = compatibility.compatibility
    return (
      <View style={styles.container}>
        <StatusBar backgroundColor="blue" barStyle="light-content" />
        <ScrollView style={{}}>
          <ImageBackground  
            source={ZodiakImages[ zodiak_sign1 ]["block"]} 
            style={{width: '100%', paddingTop: 40}} 
            resizeMode="cover"
            >
                <View style={styles.header}>
                  <IconMenu color="white" navigation={navigation} style={styles.icon_menu}/>
                  <IconShare color="white" message={this._shareText()} title={this._shareTitle()}/>
                </View>
                
                <View style={[styles.title_screen, styles.padding_content]}>
                    <Text style={styles.title_text} >
                      {i18n.t("compatibility")}
                    </Text>
                </View>

                <View style={[styles.padding_content, styles.shadow, {paddingBottom: 0,}]}>
                  <View style={styles.compatibility_block}>

                    <View style={styles.compatibility_icon}>
                      <IconCompatibility value={ rate } />
                    </View>
                    <View style={styles.zodiak_selectors}>
                      <ZodiakButton  
                        zodiak={zodiak_sign1}
                        onPress={()=>navigation.navigate('ZodiakMy')}
                      />

                      <View style={styles.borderLeft}/>

                      <ZodiakButton  
                        zodiak={zodiak_sign2} 
                        onPress={()=>navigation.navigate('ZodiakPartner')}
                      />
                    </View>
                  </View>
                  
                </View>
          </ImageBackground>

          <View style={[styles.padding_content, styles.shadow, {paddingTop: 20}]}>
            
            
                {this._parseText(texts).map((text, index) => {
                  return <View key={index} style={[styles.description_block, styles.shadow]}>
                            <Markdown style={styles} >{text}</Markdown>
                          </View>
                })}
          </View>
        </ScrollView>
      </View>
    )
  }
}

export default connect(state => ({
  user: state.user,
  compatibility: state.compatibility
}),
(dispatch) => ({
  actions: {
    user: bindActionCreators(ActionsUser, dispatch),
    compatibility: bindActionCreators(ActionsCompatibility, dispatch)
  }
})
)(CompatibilityScreen);

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#F2F2F3",
    },  
    contentContainer: {
      alignContent: "center",
      justifyContent: 'center',
      alignItems: "center",
      flex: 1
    },
    header: {
      flexDirection: "row",
      justifyContent: "space-between",
      paddingTop: 10, 
      paddingLeft: 10,
      paddingRight: 20
    },
    icon_menu: {
      left: 10
    },
    title_screen: {
      paddingTop: 50
    },  
    shadow: {
      shadowOffset:{  width: 0,  height: 10,  },
      shadowColor: '#ececec',
      shadowOpacity: 0.5,
    },
    padding_content: {
      padding: "5%",
      
    },
    title_text: {
      color: "white",
      fontWeight: "bold",
      fontSize: 36
    },
    compatibility_block: {
      backgroundColor: "white", 
      justifyContent: 'flex-end', 
      height: 200, 
      borderRadius: 16
    },
    zodiak_title: {
      flexDirection: "row",
      justifyContent: "space-between",
      paddingTop: 30,
      paddingLeft: 20,
      paddingRight: 20,
      paddingBottom: 20
    },
    zodiak_title_text: {
      fontSize: 17,
      fontWeight: "bold",
      color: Colors.black
    },
    zodiak_selectors: {
      flexDirection: "row", 
      justifyContent: "space-between",
      alignContent:"center",
      alignItems: "center"
    },
    borderLeft: {
      borderLeftWidth: 1, height: 30, borderLeftColor: "#F2F2F6",
    },
    compatibility_icon: {
      justifyContent: "center",
      alignContent: "center",
      alignItems: "center",
      top: 40
    },
    description_block: {
      backgroundColor: "white", 
      borderRadius: 16,
      padding: 30,
      paddingBottom: 20,
      paddingTop: 20,
      marginBottom: 20
    },
    heading: {
      color: Colors.grey,
      textTransform: "uppercase"
    },
    heading1: {
      fontSize: 18,
      backgroundColor: '#000000',
      color: '#FFFFFF',
    },
    heading2: {
      fontSize: 16,
    },
    heading3: {
      fontSize: 12,
    },
    heading4: {
      fontSize: 10,
    },
    heading5: {
      fontSize: 9,
    },
    heading6: {
      fontSize: 8,
    },
    text: {
      color: Colors.black,
      fontSize: 14,
      lineHeight: 23,
    }
});