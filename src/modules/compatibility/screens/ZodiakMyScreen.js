import React from 'react';
import {
  ScrollView,
  StatusBar,
  StyleSheet,
  View,
  Text,
} from 'react-native';

import i18n from '../../../i18n'

import { connect } from 'react-redux';
import {bindActionCreators} from 'redux';
import * as ActionsCompatibility from '../../../actions/Compatibility';

import IconBack from '../../../components/IconBack';
import ZodiakList from '../../horoscope/components/ZodiakList'
import Header from '../../../components/Header';
import helper from '../../../api/helper'

class ZodiakMyScreen extends React.Component {
  constructor(props) {
    super(props);
    let {user} = this.props
    this.state = { 
      
    };
  }
  static navigationOptions = {
    header: null,
  };
  render () {
    let {} = this.state
    let {navigation, actions, compatibility} = this.props
    
    return (
      <View style={styles.container}>
        <StatusBar backgroundColor="blue" barStyle="light-content" />
        <Header color="white" title={i18n.t("zodiac_sign")} navigation={navigation}/>
        
          <ZodiakList 
            style={{}} 
            onPressItem={(zodiak)=>{
              actions.compatibility.setZodiakMy(zodiak);
              actions.compatibility.getCompatibility(zodiak, compatibility.zodiak_partner);
              navigation.goBack()
            }}
            />
     </View>
    )
  }
}


export default connect(state => ({
  compatibility: state.compatibility,
}),
(dispatch) => ({
  actions: {
    compatibility: bindActionCreators(ActionsCompatibility, dispatch),
  }
})
)(ZodiakMyScreen);

const styles = StyleSheet.create({
 
  container: {
    flex: 1,
    backgroundColor: "#302F40",
    opacity: 0.95,
    paddingTop: 40,
    padding: "2%"
  },  
  contentContainer: {
    alignContent: "center",
    alignItems: "center",
    flex: 1,
    paddingTop: 40
  },
  header: {
    color: "white",
    fontSize: 24,
    fontWeight: "bold",
    textAlign: "center",
    paddingTop: 5,
    paddingBottom: 30,
},
});