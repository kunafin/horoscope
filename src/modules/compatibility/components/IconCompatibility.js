import React from 'react';
import {
    ImageBackground,
    Text,
  } from 'react-native';




export default function IconCompatibility({style, value}) {
  getImage = (value) => {
      if(value <= 24) {
        return  require("../../../assets/images/compatibility/001.png")
      } else if(value <= 49) {
        return  require("../../../assets/images/compatibility/002.png")
      } else if(value <= 69) {
        return  require("../../../assets/images/compatibility/003.png")
      }else if(value <= 89) {
        return  require("../../../assets/images/compatibility/004.png")
      }else if(value <= 100) {
        return  require("../../../assets/images/compatibility/005.png")
      }
  }
  return (
    <ImageBackground 
        style={[style, {
            width: 140, 
            height: 185, 
            alignItems: "center", 
            justifyContent: "center",
            paddingTop: 15
        }]} 
        resizeMode={"contain"}
        source={this.getImage(value)}  
    >
          <Text style={{fontSize: 32, color: "white", fontWeight: "bold"}}>{value}%</Text>
     </ImageBackground> 
  );
}
