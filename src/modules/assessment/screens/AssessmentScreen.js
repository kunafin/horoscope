import React from 'react';
import {
  Image,
  StyleSheet,
  Text,
  Platform,
  TouchableOpacity,
  Linking,
  StatusBar,
  View,
} from 'react-native';
import Colors from '../../../constants/Colors'
import { LinearGradient } from 'expo-linear-gradient';
import config from '../../../../app.json'
import IconMenu from '../../../components/IconMenu'
import i18n from '../../../i18n';

export default class AssessmentScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = { };
  }
  static navigationOptions = ({ navigation }) => {
    return {
      header: null
    }
  };
  _onSubmit() {
    
    let link = Platform.OS === 'ios' 
    ? config.link_appstore_review
    : config.link_playmarket_review
    
    Linking.canOpenURL(link).then(supported => {
        supported && Linking.openURL(link);
    }, (err) => console.log(err));
  }

  render () {
    let {navigation} = this.props
    return (
      
        <LinearGradient
            colors={["#9B82FF","#6F4CFF"]}
            style={[styles.container]}
          >
            <StatusBar barStyle="light-content" />
            <IconMenu color="white" navigation={navigation} style={{left: 10}}/>
            <View style={styles.contentContainer}>
                
                <Image source={require('../../../assets/images/stars.png')} />
                
                <Text style={styles.title}> {i18n.t("rate")} </Text>
                <Text style={styles.description}>
                  {i18n.t("rate_description")} {Platform.OS === 'ios' ? "App Store" : "Google Play"}

                </Text>
                
                <View style={[ styles.button ]} >
                  <TouchableOpacity 
                    style={[ styles.button]}
                    onPress={()=>this._onSubmit()}>
                    <Text style={[styles.button_text, ]}>
                      {i18n.t("go_to_assessment")}
                    </Text>
                  </TouchableOpacity>
                </View>
            </View> 
        </LinearGradient> 
         
    )
  }
}


const styles = StyleSheet.create({
  container: {
      flex: 1,
      padding: "4%",
      height: "100%",
      width: "100%",
      paddingTop: 50
  },  
  contentContainer: {
      alignContent: "center",
      alignItems: "center",
      justifyContent: "center",
      flex: 1,
      paddingBottom: 60,
  },
  description: {
    fontSize: 15,
    color: "white",
    textAlign: "center",
    paddingTop: 10,
    paddingBottom: 40
  },
  title: {
    fontSize: 22,
    fontWeight: "bold",
    color: "white",
    paddingTop: 40,
  },
  button: {
      backgroundColor: "white",
      borderRadius: 10, 
      height: 56,
      justifyContent: "center",
     
      width: "100%",
      alignItems: "center"
  },
  button_text: {
      color: Colors.theme,
      fontSize: 16, 
      fontWeight: "bold",
  },
  
})