import React from 'react';
import {
  Alert,
  StyleSheet,
  Text,
  TouchableOpacity,
  TextInput,
  StatusBar,
  View,
} from 'react-native';
import IconMenu from '../../../components/IconMenu'
import helper from '../../../api/helper'
import * as api from '../../../api'
import i18n from '../../../i18n';

export default class FeedbackScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = { 
      email: "",
      text: "",
    };
  }
  static navigationOptions = ({ navigation }) => {
    return {
      title: i18n.t("feedback"),
      headerLeft: <IconMenu color="black" navigation={navigation} style={{marginLeft: 20}}/>,
    }
  };
  _onSubmit() {
    let {email, text} = this.state
    let {navigation} = this.props
    api.sendFeedback(email, text).then((status) => {
      if(status == 200) {
        navigation.navigate("FeedbackSuccess")
        this.setState({text: "", email: ""})
      } else {
        Alert.alert(
          '',
          i18n.t("error_feedback") ,
        );
      }
    })
  }
  _isValidate() {
    let {email, text} = this.state
    if (helper.validateEmail(email) && text.length > 2) {
      return true;
    } 
    return false
  }
  render () {
    let isDisabled = !this._isValidate() 
    return (
      <View style={[styles.container]}>
        <StatusBar barStyle="default" />
        <View style={styles.contentContainer}>
          
            <TextInput
              style={[styles.inputText, {marginBottom: 10}]}
              onChangeText={(email) => {
                this.setState({email: email})
              }}
              underlineColorAndroid="transparent"
              value={this.state.email}
              placeholder="Email"
            />

            <TextInput
              style={[styles.inputText, {marginBottom: 10, height: 170}]}
              onChangeText={(text) => {
                this.setState({text: text})
              }}
              multiline = {true}
              numberOfLines = {4}
              underlineColorAndroid="transparent"
              value={this.state.text}
              placeholder={i18n.t("placeholder_question")} 
            />

            <View  style={[ styles.button, {opacity: (isDisabled) ? 0.3 : 1}]} >
              <TouchableOpacity 
                disabled={isDisabled}
                style={[ styles.button]}
                onPress={()=>this._onSubmit()}>
                <Text style={[styles.button_text, ]}>{i18n.t("submit")}</Text>
              </TouchableOpacity>
            </View>
        </View>    
    </View>
    )
  }
}


const styles = StyleSheet.create({
  container: {
      flex: 1,
      padding: "4%",
  },  
  contentContainer: {
      alignContent: "center",
      alignItems: "center",
      flex: 1,
      paddingBottom: 60,
  },
  inputText: {
    borderColor: 'gray', 
    borderWidth: 0, 
    backgroundColor: "#F2F2F6",
    borderRadius: 16,
    padding: 15,
    height: 56,
    paddingTop: 20,
    paddingBottom: 20, 
    fontSize: 16,
    width: "100%",
  },
  button: {
      backgroundColor: "#6F4CFF",
      borderRadius: 10, 
      height: 56,
      justifyContent: "center",
      color: "white",
      width: "100%",
      alignItems: "center"
  },
  button_text: {
      color: "white",
      fontSize: 16, 
      fontWeight: "bold",
  },
  
})