import React from 'react';
import {
  Image,
  StyleSheet,
  Text,
  TouchableOpacity,
  StatusBar,
  View,
} from 'react-native';
import Colors from '../../../constants/Colors'
import { LinearGradient } from 'expo-linear-gradient';

import i18n from '../../../i18n';

export default class FeedbackSuccessScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = { };
  }
  static navigationOptions = ({ navigation }) => {
    return {
      header: null
    }
  };
  _onSubmit() {
    let {navigation} = this.props
    navigation.navigate("Feedback")
    navigation.navigate("Horoscope")
  }

  render () {
    return (
      
        <LinearGradient
            colors={["#9B82FF","#6F4CFF"]}
            style={[styles.container]}
          >
            <StatusBar barStyle="light-content" />
            <View style={styles.contentContainer}>
                <Image source={require('../../../assets/images/mail_success.png')} />
                
                <Text style={styles.title}> {i18n.t("message_sent")} </Text>
                <Text style={styles.description}> {i18n.t("we_reply_you")} </Text>
                
                <View style={[ styles.button ]} >
                  <TouchableOpacity 
                    style={[ styles.button]}
                    onPress={()=>this._onSubmit()}>
                    <Text style={[styles.button_text, ]}>{i18n.t("ok")}</Text>
                  </TouchableOpacity>
                </View>
            </View> 
        </LinearGradient> 
         
    )
  }
}


const styles = StyleSheet.create({
  container: {
      flex: 1,
      padding: "4%",
      height: "100%",
      width: "100%"
  },  
  contentContainer: {
      alignContent: "center",
      alignItems: "center",
      justifyContent: "center",
      flex: 1,
      paddingBottom: 60,
  },
  description: {
    fontSize: 15,
    color: "white",
    paddingBottom: 40
  },
  title: {
    fontSize: 22,
    fontWeight: "bold",
    color: "white",
    paddingTop: 40,
  },
  button: {
      backgroundColor: "white",
      borderRadius: 10, 
      height: 56,
      justifyContent: "center",
     
      width: "100%",
      alignItems: "center"
  },
  button_text: {
      color: Colors.theme,
      fontSize: 16, 
      fontWeight: "bold",
  },
  
})