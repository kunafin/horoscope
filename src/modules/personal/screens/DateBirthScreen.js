import React from 'react';
import {
  Image,
  ScrollView,
  Text,
  Alert,
  TouchableOpacity,
  TextInput,
  View,
  StatusBar,
} from 'react-native';

import { connect } from 'react-redux';
import {bindActionCreators} from 'redux';
import * as ActionsUser from '../../../actions/User';

import i18n from '../../../i18n';

import Header from '../../../components/Header';
import styles from '../style';
import helper from '../../../api/helper';

class DateBirthScreen extends React.Component {
  constructor(props) {
    super(props);
    let {user} = this.props
    this.state = { 
      day:  user.day, 
      month: user.month, 
      year: user.year, 
      isDisabled: (user.day && user.month && user.year ) ? false : true 
    };
  }
  componentWillMount() {
    let { user } = this.props
    if(user.date_of_birth) {
      let date_of_birth = helper.parserDate(user.date_of_birth)
    
      this.setState({
        day : date_of_birth.day, 
        month: date_of_birth.month,
        year: date_of_birth.year,
        isDisabled: false 
      })
    }
  }
  
  _validateDate(value)
  {
    var arrD = value.split(".");
    arrD[1] -= 1;
    var d = new Date(arrD[2], arrD[1], arrD[0]);
    if ((d.getFullYear() == arrD[2]) && (d.getMonth() == arrD[1]) && (d.getDate() == arrD[0])) {
      return true;
    } else {
      return false;
    }
  }
  _onNextButton() {
    let { day, month, year } = this.state
    let {navigation, actions} = this.props
   
    if(this._validateDate(`${day}.${month}.${year}`)) 
    {
        actions.user.setUserFields({ day,  month, year})
        navigation.navigate('Sex')
    } else {
        Alert.alert(
          '',
          i18n.t("error_data_birth_day"),
        );
    }
  }
  _isValidate(field) {
    let {day, month, year} = this.state

    let mergeFields = {
      ...{day, month, year}, 
      ...field
    }
    return (mergeFields.day && mergeFields.month && mergeFields.year)
    
  }
  _onChangeText(field) {
    this.setState(field)
    this.setState({ isDisabled: (this._isValidate(field)) ? false : true})
   
  }
  render () {
    let {isDisabled} = this.state
    let {navigation} = this.props
    return (
      <View style={styles.container}>
         <StatusBar barStyle="default" />
        <ScrollView  contentContainerStyle={styles.contentContainer}>
          
          <Image source={require('../../../assets/images/calendar_big.png')}/>
          
          <Text style={styles.title}>{i18n.t("your_birth_day")}</Text>

          <Text style={styles.description}>{i18n.t("step")} 2 {i18n.t("of")} 3</Text>
          <View style={{flexDirection: "row", justifyContent: 'space-between'}}>

          <TextInput
              style={[styles.inputText, styles.inputDate]}
              onChangeText={(day) => this._onChangeText({day})}
              underlineColorAndroid="transparent"
              value={this.state.day}
              maxLength={2}
              placeholder={i18n.t("dd")}
              keyboardType="numeric"
            />

            <TextInput
              style={[styles.inputText, styles.inputDate]}
              onChangeText={(month) => this._onChangeText({month})}
              underlineColorAndroid="transparent"
              value={this.state.month}
              placeholder={i18n.t("mm")}
              maxLength={2}
              keyboardType="numeric"
            />

            <TextInput
              style={[styles.inputText, styles.inputDate, {marginRight: 0, width: "42%" }]}
              onChangeText={(year) => this._onChangeText({year})}
              underlineColorAndroid="transparent"
              value={this.state.year}
              placeholder={i18n.t("yyyy")}
              maxLength={4}
              keyboardType="numeric"
            />

          </View>
            
            <View  style={[ styles.button, {opacity: (isDisabled) ? 0.3 : 1}]} >
              <TouchableOpacity 
                disabled={isDisabled}
                style={[ styles.button]}
                onPress={()=>this._onNextButton()}>
                <Text style={[styles.button_text, ]}>{i18n.t("continue")}</Text>
              </TouchableOpacity>
            </View>
        </ScrollView>   
        <Header 
          color="black" 
          title="" navigation={navigation} 
          style={{marginBottom: 0, paddingTop: 35, position: "absolute"}}
          /> 
    </View>
    )
  }
}


DateBirthScreen.navigationOptions = {
  header: null,
};

export default connect(state => ({
  user: state.user,
}),
(dispatch) => ({
  actions: {
    user: bindActionCreators(ActionsUser, dispatch),
  }
})
)(DateBirthScreen);
