import React from 'react';
import {
  Image,
  StatusBar,
  Text,
  TouchableOpacity,
  TextInput,
  View,
} from 'react-native';

import { connect } from 'react-redux';
import {bindActionCreators} from 'redux';
import * as ActionsUser from '../../../actions/User';
import i18n from '../../../i18n';

import styles from '../style'

class UsernameScreen extends React.Component {
  constructor(props) {
    super(props);
    let {user} = this.props
    this.state = { 
      name: user.name, 
      isDisabled: user.name ? false : true 
    };
  }
  static navigationOptions = {
    header: null,
  };
  _onNextButton() {
    let {isDisabled, name} = this.state
    let {navigation, actions} = this.props
    if(!isDisabled) {
      actions.user.setUserFields({ name })
      navigation.navigate('DateBirth')
    }
  }
  
  render () {
    let {isDisabled} = this.state
    return (
      <View style={[styles.container]}>
        <StatusBar barStyle="default" />
        <View style={styles.contentContainer}>
          <Image source={require('../../../assets/images/profile_big.png')}/>
          
          <Text style={styles.title}> {i18n.t("how_your_name")} </Text>

          <Text style={styles.description}> {i18n.t("step")} 1 {i18n.t("of")} 3</Text>

            <TextInput
              style={[styles.inputText, {marginBottom: 10}]}
              onChangeText={(name) => {
                let disabled = name !="" ? false : true;
                this.setState({name: name, isDisabled: disabled})
              }}
              underlineColorAndroid="transparent"
              value={this.state.name}
              placeholder={i18n.t("enter_your_name")}
            />
            <View  style={[ styles.button, {opacity: (isDisabled) ? 0.3 : 1}]} >
              <TouchableOpacity 
                disabled={isDisabled}
                style={[ styles.button]}
                onPress={()=>this._onNextButton()}>
                <Text style={[styles.button_text, ]}>{i18n.t("continue")}</Text>
              </TouchableOpacity>
            </View>
        </View>    
    </View>
    )
  }
}



export default connect(state => ({
  user: state.user,
}),
(dispatch) => ({
  actions: {
    user: bindActionCreators(ActionsUser, dispatch),
  }
})
)(UsernameScreen);
