import React from 'react';
import {
  Image,
  ScrollView,
  Text,
  TouchableOpacity,
  View,
  StatusBar,
} from 'react-native';
import { LinearGradient } from 'expo-linear-gradient';

import { connect } from 'react-redux';
import {bindActionCreators} from 'redux';
import * as ActionsUser from '../../../actions/User';

import IconChecked from '../../../components/IconChecked';
import Header from '../../../components/Header';
import styles from '../style'

import i18n from '../../../i18n';

class SexScreen extends React.Component {
  constructor(props) {
    super(props);
    let {user} = this.props
    this.state = { 
      isDisabled: user.gender ? false : true,
      gender: user.gender,
      sex: [
        {
          title: i18n.t("famale"),
          value: 'female', 
          img: require('../../../assets/images/venus.png'),
          linearGradient: ['#FF72B1', '#FF4B9C']
        },
        {
          title: i18n.t("male"), 
          value: 'male', 
          img: require('../../../assets/images/mars.png'),
          linearGradient: ['#6B80FF', '#4C66FF']
        }
      ]
    };
  }
  
  _onNextButton() {
    let {isDisabled, gender} = this.state
    let {navigation, actions, user} = this.props
    if(!isDisabled) {
      user.gender = gender
      actions.user.setUser(user)
      if(navigation.state.params != undefined) {
        let {navigate} = navigation.state.params
        navigation.navigate(navigate ? navigate : "Horoscope")
      } else {
        navigation.navigate('Horoscope')
      }
    }
  }
  _onSexChecked(value) {
    this.setState({gender: value, isDisabled: false})
  }
  renderSex (item) {
    let {gender} = this.state
    let {user} = this.props
    
    return (
      <TouchableOpacity 
        style={{width: "100%"}} 
        key={item.value} 
        onPress={()=>this._onSexChecked(item.value)}
      >
        <LinearGradient
            colors={item.linearGradient}
            
            style={styles.sex_block}
          >
            <IconChecked checked={(gender == item.value) ? true : false} style={styles.sex_check}/>
            <Text style={styles.title_sex}>{item.title}</Text>
            <Image style={styles.sex_image} source={item.img} />

        </LinearGradient>
      </TouchableOpacity>
    )
  }
  render () {
    let {isDisabled, sex} = this.state
    let {navigation} = this.props

    return (
      <View style={styles.container} >
        <StatusBar barStyle="default" />
        <ScrollView  contentContainerStyle={styles.contentContainer}>
        
          <Image source={require('../../../assets/images/sex_big.png')}/>
          
          <Text style={styles.title}>
            {i18n.t("how_gender")}
          </Text>
          <Text style={styles.description}>{i18n.t("step")} 3 {i18n.t("of")} 3</Text>

          {sex.map((item) => this.renderSex(item))}
          
          <View  style={[ styles.button, {opacity: (isDisabled) ? 0.3 : 1}]} >
            <TouchableOpacity 
              disabled={isDisabled}
              style={[ styles.button]}
              onPress={()=>this._onNextButton()}>
              <Text style={[styles.button_text, ]}>{i18n.t("continue")}</Text>
            </TouchableOpacity>
          </View>
        </ScrollView>
        <Header 
          color="black" 
          title="" navigation={navigation} 
          style={{marginBottom: 0, paddingTop: 35, position: "absolute"}}
          />
      </View>
    )
  }
}


SexScreen.navigationOptions = {
  header: null,
};

export default connect(state => ({
  user: state.user,
}),
(dispatch) => ({
  actions: {
    user: bindActionCreators(ActionsUser, dispatch),
  }
})
)(SexScreen);
