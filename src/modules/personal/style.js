import {
    StyleSheet,
  } from 'react-native';

export default StyleSheet.create({
    container: {
        flex: 1,
        padding: "2%",
        paddingTop: 40,
    },  
    contentContainer: {
        alignContent: "center",
        alignItems: "center",
        flex: 1,
        padding: "2%",
        paddingBottom: 60,
    },
    inputDate: {
      marginBottom: 10, 
      width: "25%", 
      marginRight: "2%", 
      textAlign: "center"
    },
    title: {
        fontSize: 22,
        fontWeight: "bold",
        marginTop: 20,
        marginBottom: 10
    },
    description: {
        fontSize: 18,
        color: "#ADADC0",
        marginBottom: 25
    },
    inputText: {
      height: 40, 
      borderColor: 'gray', 
      borderWidth: 0, 
      backgroundColor: "#F2F2F6",
      borderRadius: 16,
      padding: 15,
      height: 56,
      paddingTop: 20,
      paddingBottom: 20, 
      width: "100%"
    },
    button: {
        backgroundColor: "#6F4CFF",
        borderRadius: 10, 
        height: 56,
        justifyContent: "center",
        color: "white",
        width: "100%",
        alignItems: "center"
    },
    button_text: {
        color: "white",
        fontSize: 16, 
        fontWeight: "bold",
    },
    title_sex: {
        color: "white",
        fontSize: 22,
        fontWeight: "bold"
    },
    sex_block: { 
        width: "100%",
        marginBottom: 20, 
        borderRadius: 16,
        height: 120, 
        flexDirection: "row", 
        alignItems: "center"
    },
    sex_image: {
        bottom: 0, 
        position: "absolute", 
        right: 20
    },
    sex_check: {
        paddingLeft: 30, 
        paddingRight: 25
    }
})