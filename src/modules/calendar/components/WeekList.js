import React from 'react';
import {
  Image,
  Platform,
  FlatList,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import helper from '../../../api/helper'
import Colors from '../../../constants/Colors';
import i18n from '../../../i18n'

export default class WeekList extends React.Component {
    constructor(props) {
        super(props);
        let {minDate, maxDate, selectDate} = this.props
        this.state = { 
            locale: i18n.t("locale"),
            weeks: [],
            minDate : minDate ? this.getWeekNumber(new Date(minDate)) : "",
            maxDate: maxDate ? this.getWeekNumber(new Date(maxDate)) : "",
            selected: selectDate ? this.getWeekNumber(new Date(selectDate)) : "",
            current: this.getWeekNumber(new Date(Date.now())),
        };
    }
    componentDidMount() {
        let {maxDate, minDate} = this.state

        
        let maxWeek = new Date( maxDate.startWeekDay ); 
        //Включит текущую неделю в список, если в настройках она оказалась крайней
        maxWeek.setDate(maxWeek.getDate() + 7)
        this.addWeeks(maxWeek)
    }
    
    addWeeks(date = "") {
        let {weeks, minDate, maxDate} = this.state
        for (i=0; i < 52; i++) {
            if(!date) {
                date = new Date(Date.now());
            } 
            else {
                date = new Date(date - 86400 * 1000 * 7)
            }
            let item = this.getWeekNumber(date);
            
            weeks.push(item);
            
            if(this.compare(minDate, item)) {
                //останавливает как только список касаеся минимальной даты
                break;
            }
        }
        this.setState({weeks});
    }
    getWeekNumber(d) {
        return helper.getWeek(d);
    }
    _isCurrentWeek(item) {
        //add marker this week 
        let {current} = this.state
        if(this.compare(current, item)) {
            return true;
        }
        return false;
    }
   
    _onScrollTop() {
       
        let {weeks, minDate} = this.state;
        let week = weeks[weeks.length - 1];
        let last_date = week.endWeekDay;
        if(this.compare(minDate, week)) {
            //stop 
            return false;
        }
        this.addWeeks(last_date)
    }
   
    compare(check_item, item) {
        if(check_item.weekNo == item.weekNo && check_item.year == item.year) {
            return true;
        }
        return false;
    }
    _onSelect(item) {
        this.setState({selected: item})
        let {navigation, actions, zodiak} = this.props
        
        let date = item.startWeekDay;
        actions.calendar.setCalendar('week', date)

        if(this._isCurrentWeek(item)) {
            navigation.navigate({
                routeName: 'Horoscope',
                params: {
                  type: "week"
                },
                key: "week"
            })
            actions.horoscope.delHoroscope('custom')
        } else {
            actions.horoscope.getCustomeHoroscope("week", date, zodiak)
            navigation.navigate('Horoscope', {
                type: "custom"
            })
        }
       
    }

    _renderItem(item) {
        let {selected, locale} = this.state;
        let isSelected = this.compare(selected, item);
        return (
            <TouchableOpacity 
                style={[{paddingLeft: 15, paddingRight: 15}, isSelected ? styles.selectedItem : null]}
                onPress={()=>this._onSelect(item)}    
            >
                <View style={[styles.itemWeek,  isSelected ? {borderTopWidth: 0} : null]}>
                    <Text style={[styles.date, isSelected ? styles.dateSelect : null]}>
                        {item.weekNo}
                    </Text>
                    <Text style={[styles.date, isSelected ? styles.dateSelect: null, {width: "80%"}]}>
                        {helper.getFullDate(item.startWeekDay, locale) } — {helper.getFullDate( item.endWeekDay, locale )}
                    </Text>
                    <View>
                        <Text style={ [
                            this._isCurrentWeek(item) 
                            ? styles.marker 
                            : {opacity: 0}, 
                            {color: isSelected ? "white" : Colors.theme}] }>·</Text>
                    </View>
                </View>
            </TouchableOpacity>
        )
    }
    _keyExtractor(item) {
        return item.weekNo + "_" +item.year;
    }
    // getItemLayout = (data, index) => (
    //     { length: 50, offset: 50 * index, index }
    // )
    render() {
        let {weeks, selected} = this.state;

        console.log('selected', selected)
        return (
        <View >
            <FlatList
                style={{padding: 10}}
                inverted
                //getItemLayout={this.getItemLayout}
                data={weeks}
                //initialScrollIndex={1}
                onEndReached = { () => this._onScrollTop()}
                onEndReachedThreshold={0.9}
                keyExtractor={(item) => this._keyExtractor(item)}
                renderItem={({item}) => this._renderItem(item)}
                />
        </View>
        )
    }
}


const styles = StyleSheet.create({
   itemWeek: {
        flexDirection: "row", 
        justifyContent: "space-between",
        width: "100%",
        padding: 10, 
        paddingLeft: 0, 
        borderTopWidth: 0.5, 
        borderTopColor: "#F2F2F6"
   },
   marker : {
        fontSize: 60,  
        lineHeight: Platform.OS === 'ios' ? 38 : 48 , 
        position: "absolute",
        left: 0
   },
   selectedItem: {
        backgroundColor: Colors.theme, 
        borderRadius: 5,
        marginTop: 1,
        marginBottom: -1
   },
   date: {
       textAlign: "left",
       color: Colors.black
   }, 
   dateSelect: {
       color: "white"
   }
});
