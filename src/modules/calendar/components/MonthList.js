import React from 'react';
import {
  Image,
  Platform,
  FlatList,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import helper from '../../../api/helper'
import Colors from '../../../constants/Colors';
import Locale from '../../../constants/Locale'

import i18n from '../../../i18n';

export default class MonthList extends React.Component {
    constructor(props) {
        super(props);
        let {minDate, maxDate, selectDate} = this.props

        this.state = { 
            minDate : minDate ? new Date(minDate).getFullYear() : null,
            maxDate: maxDate ? new Date(maxDate).getFullYear() : null,
            selected: selectDate ? this.firstDayMonth(selectDate) : null,
            current: this.firstDayMonth(new Date(Date.now())) ,
            //currentYear: new Date(Date.now()).getFullYear(),
            //currentMonth: new Date(Date.now()).getMonth(),
            years: [],
            months:  Locale[i18n.t('locale')]["monthNames"],
            
        };


    }
    componentDidMount() {
        this._addYears()
    }
    firstDayMonth(date) {
        var d = new Date(date),
            month = '' + (d.getMonth() + 1),
            day = '1',
            year = d.getFullYear();
    
        if (month.length < 2) month = '0' + month;
        if (day.length < 2) day = '0' + day;
    
        return [year, month, day].join('-');
    }
    _addYears() {
        let {minDate, maxDate, current, selected} = this.state;
        let years = [];
        let year_select = new Date(selected).getFullYear()
        if(minDate && maxDate) {
            for ( i = maxDate; i >= minDate ; i--) {
                years.push(i);
            }
        } else {
            years.push(new Date(current).getFullYear());
        }

        this.setState({years: years})
    }
    _isCurrentMonth(month, year) {
        let {current} = this.state
        
        if(this._decodeMonth({month, year}) == current) {
            return true
        }
    }
    _onSelect(month, year) {
        let {navigation, actions, zodiak} = this.props
        let date = this._decodeMonth({month, year})
        this.setState({selected: date})
        this. _addYears()
        actions.calendar.setCalendar('month', date)

        if(this._isCurrentMonth( month, year )) {
            navigation.navigate({
                routeName: 'Horoscope',
                params: {
                  type: "month"
                },
                key: "month"
              })
              actions.horoscope.delHoroscope('custom')
        } else {
            actions.horoscope.getCustomeHoroscope("month", date, zodiak)
            navigation.navigate('Horoscope', {
                type: "custom"
            })
        }
        
    }
    _decodeMonth({month, year}) {
        let {months} = this.state
        let number_mount = "0";
        months.find((item, index) => {
            if( item == month ) {
                number_mount = index
            }
        })
        return this.firstDayMonth(new Date(year, number_mount));
    }
    _isIncludedRangeDate (date, minDate, maxDate) {
        return new Date(minDate) <= new Date(date) && new Date(date) <= new Date(maxDate)
     }
    _rendermonths(month, year) {
        let {selected} = this.state
        let isSelected = this._decodeMonth({month, year}) == selected;
        let {minDate, maxDate} = this.props
        if(!this._isIncludedRangeDate(this._decodeMonth({month, year}), minDate, maxDate)) {
            return null
        }
        return (
            <TouchableOpacity 
                key={month.toString()}
                style={[{paddingLeft: 15, paddingRight: 15}, isSelected ? styles.selectedItem : null]}
                onPress={()=>this._onSelect(month, year)}    
            >
                <View style={[styles.itemYear,  (isSelected ) ? {borderTopWidth: 0, } : null]}>
                    <Text style={[styles.date, isSelected ? styles.dateSelect : null]}>
                        {month}
                    </Text>
                    <View>
                        <Text style={ [
                            this._isCurrentMonth(month, year) 
                            ? styles.marker 
                            : {opacity: 0}, 
                            {color: isSelected ? "white" : Colors.theme}] }>·</Text>
                    </View>
                </View>
            </TouchableOpacity>
        )
    }
    _renderItem(year) {
        let {selected, months} = this.state;
        return (
            <View style={{
                padding: 10, 
                paddingTop: 20, 
                paddingBottom: 20,  
                backgroundColor: "white", 
                borderRadius: 16, 
                marginBottom: 20
            }}>
                <Text style={styles.title}>{year}</Text>
                {months.map((month, index) => this._rendermonths(month, year) )}
            </View>
        )
    }
    
    render() {
        let {years,  selected} = this.state;
        let {style} = this.props
        
        return (
                <FlatList
                    style={style}
                    inverted
                    //getItemLayout={this.getItemLayout}
                    data={years}
                    extraData={selected}
                    //initialScrollIndex={1}
                    //onEndReached = { () => this._onScrollTop()}
                    onEndReachedThreshold={0.9}
                    keyExtractor={(item, index) => index.toString()}
                    renderItem={({item}) => this._renderItem(item)}
                />
        
        )
    }
}


const styles = StyleSheet.create({
 
   itemYear: {
        flexDirection: "row", 
        justifyContent: "space-between",
        width: "100%",
        padding: 10, 
        paddingLeft: 0, 
        borderTopWidth: 0.5, 
        borderTopColor: "#F2F2F6"
   },
   marker : {
        fontSize: 60,  
        lineHeight: Platform.OS === 'ios' ? 38 : 48 , 
        position: "absolute",
        right: 0
   },
   selectedItem: {
        backgroundColor: Colors.theme, 
        borderRadius: 5,
   },
   date: {
        textAlign: "left",
        color: Colors.black
   }, 
   dateSelect: {
        color: "white"
   },
   title: {
        color: Colors.grey,
        fontSize: 10,
        paddingLeft: 15,
        paddingBottom: 10
   }
});
