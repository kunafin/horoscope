import React from 'react';
import {
  Image,
  Platform,
  FlatList,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import Colors from '../../../constants/Colors';

export default class YearList extends React.Component {
    constructor(props) {
        super(props);
        let {minDate, maxDate, selectDate} = this.props

        this.state = { 
            minDate : minDate ? new Date(minDate).getFullYear() : null,
            maxDate: maxDate ? new Date(maxDate).getFullYear() : null,
            selected: selectDate ? new Date(selectDate).getFullYear() : null,
            current: new Date(Date.now()).getFullYear(),
            years: []
        };
    }
    componentDidMount() {
        let {minDate, maxDate, current} = this.state;
        
        let years = [];
        if(minDate && maxDate) {
            for ( i = minDate; i <= maxDate ; i++) {
                years.push(i);
            }
        } else {
            years.push(current);
        }
        
        this.setState({years: years})
    }
    
    _isCurrentYear(item) {
        let {current} = this.state
        if(item == current) {
            return true
        }
    }
    _onSelect(year) {
        let {navigation, actions, zodiak} = this.props
        this.setState({selected: year})
        let date = new Date(Date.UTC(year, 0, 1));
        
        actions.calendar.setCalendar('year', date)

        if(this._isCurrentYear(year)) {
            navigation.navigate({
                routeName: 'Horoscope',
                params: {
                  type: "year"
                },
                key: "year"
            })
            actions.horoscope.delHoroscope('custom')
        } else {
            actions.horoscope.getCustomeHoroscope("year", date, zodiak)
            navigation.navigate('Horoscope', {
                type: "custom"
            })
        }
       
    }
    compare(compate_year, year) {
        if(compate_year == year) {
            return true;
        }
        return false;
    }
    _renderItem(item) {
        let {selected, years} = this.state;
        let isSelected = this.compare(selected, item);
        let isFirstItem = item == years[0]
        return (
            <TouchableOpacity 
                key={item.toString()}
                style={[{paddingLeft: 15, paddingRight: 15}, isSelected ? styles.selectedItem : null]}
                onPress={()=>this._onSelect(item)}    
            >
                <View style={[styles.itemYear,  (isSelected || isFirstItem) ? {borderTopWidth: 0, } : null]}>
                    <Text style={[styles.date, isSelected ? styles.dateSelect : null]}>
                        {item}
                    </Text>
                    <View>
                        <Text style={ [
                            this._isCurrentYear(item) 
                            ? styles.marker 
                            : {opacity: 0}, 
                            {color: isSelected ? "white" : Colors.theme}] }>·</Text>
                    </View>
                </View>
            </TouchableOpacity>
        )
    }
    
    render() {
        let {years, selected} = this.state;

        return (
        <View style={{padding: 10, paddingTop: 20, paddingBottom: 20,  backgroundColor: "white", borderRadius: 16}}>
            {years.map((item, index) => {
                return this._renderItem(item)
            })}
        </View>
        )
    }
}


const styles = StyleSheet.create({
 
   itemYear: {
        flexDirection: "row", 
        justifyContent: "space-between",
        width: "100%",
        padding: 10, 
        paddingLeft: 0, 
        borderTopWidth: 0.5, 
        borderTopColor: "#F2F2F6"
   },
   marker : {
        fontSize: 60,  
        lineHeight: Platform.OS === 'ios' ? 38 : 48 , 
        position: "absolute",
        right: 0
   },
   selectedItem: {
        backgroundColor: Colors.theme, 
        borderRadius: 5,
   },
   date: {
       textAlign: "left",
       color: Colors.black
   }, 
   dateSelect: {
       color: "white"
   }
});
