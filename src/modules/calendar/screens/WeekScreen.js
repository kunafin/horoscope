import React from 'react';
import {
  Image,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';

import i18n from '../../../i18n';

import { connect } from 'react-redux';
import {bindActionCreators} from 'redux';
import * as ActionsCalendar from '../../../actions/Calendar';
import * as ActionsHoroscope from '../../../actions/Horoscope';
import * as ActionsSetting from '../../../actions/Setting';

import WeekList from '../components/WeekList'

class WeekScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = { 
    
    };
  }
  
  render() {
    let {navigation, actions, calendar, horoscope} = this.props
    let selectDate = (calendar.type == "week") ? calendar.date : false
    
    let {setting} = this.props
    let {min_date, max_date} = setting.calendar.find((item) => item.period == "week")
    return (
      <View style={styles.container}>
        <ScrollView
          style={styles.container}
          contentContainerStyle={styles.contentContainer}>
          <WeekList 
            minDate={min_date} 
            maxDate={max_date}
            selectDate={selectDate}
            navigation={navigation}
            actions={actions}
            zodiak={horoscope.zodiak}
          />
        </ScrollView>

      </View>
    )
  }
}

WeekScreen.navigationOptions = ({navigation}) => {
  return {
    tabBarLabel: i18n.t("WEEKS"),
  }
}

export default connect(state => ({
  calendar: state.calendar,
  horoscope: state.horoscope,
  setting: state.setting
}),
(dispatch) => ({
  actions: {
    calendar: bindActionCreators(ActionsCalendar, dispatch),
    horoscope: bindActionCreators(ActionsHoroscope, dispatch),
    setting: bindActionCreators(ActionsSetting, dispatch),
  }
})
)(WeekScreen);

const styles = StyleSheet.create({
    container: {
        flex: 1
    },  
    contentContainer: {
      alignContent: "center",
      justifyContent: 'center',
      alignItems: "center",
      flex: 1
  }
});
