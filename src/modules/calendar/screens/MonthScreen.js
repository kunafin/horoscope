import React from 'react';
import {
  Image,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';

import i18n from '../../../i18n';

import { connect } from 'react-redux';
import {bindActionCreators} from 'redux';
import * as ActionsCalendar from '../../../actions/Calendar';
import * as ActionsHoroscope from '../../../actions/Horoscope';
import * as ActionsSetting from '../../../actions/Setting';

import MonthList from '../components/MonthList'

class MonthScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = { 
    
    };
  }
  
  render() {
    let {navigation, actions, calendar, horoscope} = this.props
    let selectDate = (calendar.type == "month") ? calendar.date : false
    
    let {setting} = this.props
    let {min_date, max_date} = setting.calendar.find((item) => item.period == "month")
    return (
      <View style={styles.container}>
          <MonthList 
            minDate={min_date}
            maxDate={max_date}
            selectDate={selectDate}
            style={{padding: "5%"}}
            navigation={navigation}
            actions={actions}
            zodiak={horoscope.zodiak}
          />
      </View>
    )
  }
}
MonthScreen.navigationOptions = ({navigation}) => {
  return {
    tabBarLabel: i18n.t("MONTHS"),
  }
}

export default connect(state => ({
  calendar: state.calendar,
  horoscope: state.horoscope,
  setting: state.setting
}),
(dispatch) => ({
  actions: {
    calendar: bindActionCreators(ActionsCalendar, dispatch),
    horoscope: bindActionCreators(ActionsHoroscope, dispatch),
    setting: bindActionCreators(ActionsSetting, dispatch),
  }
})
)(MonthScreen);

const styles = StyleSheet.create({
  container: {
      flex: 1,
      backgroundColor: "#F2F2F2",
  },  
  contentContainer: {

  }
});
