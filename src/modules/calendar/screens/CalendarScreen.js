import React from 'react';
import {
  StyleSheet,
} from 'react-native';
import { createMaterialTopTabNavigator, createStackNavigator } from 'react-navigation';
import i18n from '../../../i18n'

import Colors from '../../../constants/Colors'
import IconBack from '../../../components/IconBack'

import DayScreen from './DayScreen';
import WeekScreen from './WeekScreen';
import MonthScreen from './MonthScreen';
import YearScreen from './YearScreen';

const CalendarTabScreen = createMaterialTopTabNavigator(
  {
    Day: { screen: DayScreen },
    Week: { screen: WeekScreen },
    Month: { screen: MonthScreen },
    Year: { screen: YearScreen },
  },
  {
    tabBarPosition: 'top',
    swipeEnabled: true,
    animationEnabled: true,
    
    tabBarOptions: {
      activeTintColor: Colors.theme,
      inactiveTintColor: 'black',
      style: {
        backgroundColor: '#FFFFFF',
      },
      labelStyle: {
        fontSize: 10,
        textAlign: 'center',
      },
      indicatorStyle: {
        borderBottomColor: Colors.theme,
        borderBottomWidth: 2,
      },
    },
  }
);

CalendarTabScreen.navigationOptions = ({navigation}) => {
  return {
    title: i18n.t("calendare"),
    headerLeft: <IconBack color="black" navigation={navigation} style={{left: 15}}/>,
    headerStyle: {
      borderBottomColor: "transparent",
      borderBottomWidth: 0,
      elevation: 0,
    }
  }
};
export default CalendarTabScreen;


const styles = StyleSheet.create({
    container: {
        flex: 1
    },  
    contentContainer: {
      alignContent: "center",
      justifyContent: 'center',
      alignItems: "center",
      flex: 1
  }
});
