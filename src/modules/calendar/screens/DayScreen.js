import React from 'react';
import {
  Image,
  Platform,
  ScrollView,
  StyleSheet,
  StatusBar,
  TouchableOpacity,
  View,
} from 'react-native';

import i18n from '../../../i18n';
import * as api from '../../../api'
import { connect } from 'react-redux';
import {bindActionCreators} from 'redux';
import * as ActionsCalendar from '../../../actions/Calendar';
import * as ActionsHoroscope from '../../../actions/Horoscope';
import * as ActionsSetting from '../../../actions/Setting';

import {CalendarList, LocaleConfig} from 'react-native-calendars';
import Colors from '../../../constants/Colors';
import helper from '../../../api/helper';
import Locale from '../../../constants/Locale';

LocaleConfig.locales['ru'] = {
  monthNames: Locale["ru"].monthNames,
  monthNamesShort: Locale["ru"].monthNamesShort,
  dayNames: Locale["ru"].dayNames,
  dayNamesShort: Locale["ru"].dayNamesShort,
  today: Locale["ru"].today,
};


class DayScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = { 
      markedDates: this._getMarkedToday(),
      min_date: null, 
      max_date: null, 
    };
  }
  
  componentWillMount() {
    
    let {navigation, actions, calendar, setting} = this.props
    if(setting.locale == "ru") {
      LocaleConfig.defaultLocale = setting.locale;
    }
    let selectDate = (calendar.type == "day") ? calendar.date : false
    if(selectDate) {
      this._addMarkerDay(selectDate)
    }
   
  }


  componentDidMount() {
    let {setting} = this.props
    
    if(setting.calendar) {
      let {min_date, max_date} = setting.calendar.find((item) => item.period == "day")

      this.setState({min_date, max_date})
    }
  }
  _getMarkedToday() {
    let arrayDate = {};
    //Ставим маркер на сегодняшнюю дату
    arrayDate[ helper.formatDate(new Date) ] = {
      marked: true, 
      activeOpacity: 0, 
      dotColor: Colors.theme
    }
    return arrayDate;
  }
  
  _addMarkerDay(day) {
    markedDates = {};
    markedDates = this._getMarkedToday();
    
    markedDates[ day.dateString ] = {
      selected: true, 
      marked: (helper.formatDate(new Date) == day.dateString) ? true : false, 
      selectedColor: Colors.theme, 
      dotColor: Colors.theme
    };
    this.setState({markedDates})
  }
  
  _onSelectDay(day) {
    let {navigation, actions, calendar, horoscope} = this.props
    this._addMarkerDay(day)
    actions.calendar.setCalendar('day', day)

    if((helper.formatDate(new Date) == day.dateString)) {
      //выбран сегодняшний день
      navigation.navigate({
        routeName: 'Horoscope',
        params: {
          type: "today"
        },
        key: "today"
      })
      actions.horoscope.delHoroscope('custom')
    } else {
      actions.horoscope.getCustomeHoroscope("day", day.dateString, horoscope.zodiak)
      navigation.navigate('Horoscope', {
          type: "custom"
      })
    }
    
  }
  
  render() {
    let {markedDates, min_date, max_date} = this.state
    
    return (
      <View style={styles.container}>
        
        <StatusBar barStyle="default" />
        <CalendarList 
          // Minimum date that can be selected, dates before minDate will be grayed out. Default = undefined
          minDate={min_date}
          // Maximum date that can be selected, dates after maxDate will be grayed out. Default = undefined
          maxDate={max_date}
          monthFormat={'MMMM yyyy'}
          scrollEnabled={true}
          markedDates={{ ...markedDates }}
          // Handler which gets executed on day press. Default = undefined
          onDayPress={(day) => {console.log('onDayPress selected day', day);  this._onSelectDay(day)}}
          // Handler which gets executed on day long press. Default = undefined
          onDayLongPress={(day) => {console.log('onDayLongPress selected day', day); this._onSelectDay(day)}}
          // Handler which gets executed when visible month changes in calendar. Default = undefined
          onMonthChange={(month) => {console.log('month changed', month)}}
           // If firstDay=1 week starts from Monday. Note that dayNames and dayNamesShort should still start from Sunday.
          firstDay={1}
           // Callback which gets executed when visible months change in scroll view. Default = undefined
          //onVisibleMonthsChange={(months) => {console.log('Смена заголовка', months);}}
          //hideDayNames={true}
          theme={{
            todayTextColor: Colors.theme,
            selectedDayBackgroundColor: 'red',
            
            'stylesheet.calendar.header': {
              monthText: {
                textTransform: "uppercase",
                marginBottom: 10,
                color: Colors.grey,
              },
              header: {
                flexDirection: 'row',
                justifyContent: 'space-between',
                paddingLeft: 10,
                paddingRight: 10,
                marginTop: 6,
                alignItems: 'center',
              },
            },
            'stylesheet.day.basic' : {
              dot: {
                width: 8,
                height: 8,
                marginTop: 5,
                borderRadius: 4,
                opacity: 0
              },
              selectedDot: {
                backgroundColor: Colors.theme,
                marginTop: 10,
              },
              todayDot: {
                backgroundColor: Colors.theme
              }
            },
            
          }}
          />

      </View>
    )
  }
}

DayScreen.navigationOptions = ({navigation}) => {
  return {
    tabBarLabel: i18n.t("DAYS"),
  }
}
export default connect(state => ({
  calendar: state.calendar,
  horoscope: state.horoscope,
  setting: state.setting
}),
(dispatch) => ({
  actions: {
    calendar: bindActionCreators(ActionsCalendar, dispatch),
    horoscope: bindActionCreators(ActionsHoroscope, dispatch),
    setting: bindActionCreators(ActionsSetting, dispatch),

  }
})
)(DayScreen);

const styles = StyleSheet.create({
    container: {
        flex: 1
    },  
    contentContainer: {
      alignContent: "center",
      justifyContent: 'center',
      alignItems: "center",
      flex: 1
  }
});
