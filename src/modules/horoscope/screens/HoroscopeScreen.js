import React from 'react';
import {
  Platform,
  Text,
  StatusBar,
  StyleSheet,
  TouchableOpacity,
  View,
} from 'react-native';
import { LinearGradient } from 'expo-linear-gradient';
import Swiper from 'react-native-swiper' 


import { connect } from 'react-redux';
import {bindActionCreators} from 'redux';
import * as ActionsUser from '../../../actions/User';
import * as ActionsHoroscope from '../../../actions/Horoscope';
import * as ActionsSetting from '../../../actions/Setting';

import Colors from '../../../constants/Colors'
import { Feather} from '@expo/vector-icons';

import Loading from '../../../components/Loading'

import TextZodiak from '../../../components/TextZodiak';
import IconShare from '../../../components/IconShare'
import HoroscopeItem from '../components/HoroscopeItem';

import helper from '../../../api/helper'

class HoroscopeScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = { 
      slide_start_index: null,
      slide_index: null,
      dates: [],
      end_slide_index: null,
    };
  }
  static navigationOptions = {
    header: null,
  };
  componentWillMount() {
    let {actions, user, setting, horoscope} = this.props
    let zodiak = horoscope.zodiak ? horoscope.zodiak : user.zodiak_sign
    actions.setting.getLocale()
    actions.horoscope.setZodiak(zodiak)
    this.refreshSlider()

    this._interval = setInterval(() => { //обновление гороскопа, если он устарел
      let today = this._findHoroscope('today')
      if(today) {
        if(helper.formatDate(today.date) != helper.formatDate(new Date)) {
          this._allUpdateHoroscopes()
         }
      }
    }, 5000);
  }
 
  componentWillUnmount() {
    clearInterval(this._interval);
  }
  refreshSlider() {
    let dates = this.dateList();
    let {navigation} = this.props
    
    let start_screen = "today"
    if(navigation.state.params) {
      let {type} = navigation.state.params
      start_screen = (type != "custom") ? type : start_screen;

      console.log('refreshSlider', start_screen)
    } 
   

    let slide_start_index = dates.map((item, index) => {
      return item.name
    }).indexOf(start_screen)
    
    this.setState({
      dates, 
      slide_start_index, 
      slide_index: slide_start_index,
      end_slide_index: dates.length - 1
    })
  }
  componentDidUpdate(prevProps) {
    const { user, horoscope, actions, navigation} = this.props;
    if(prevProps.user !== user) {
      //поменялся zodiak пользователя (дата рождения)
      actions.horoscope.setZodiak(user.zodiak_sign)
      actions.horoscope.refreshHoroscope()
      actions.setting.getSettingCalendar()
    }
    if(prevProps.horoscope.zodiak !== horoscope.zodiak) {
      //обновляем имеющиеся гороскопы при изменении зодиака
      if(horoscope.list.length) {
        this._allUpdateHoroscopes()
      } else {
        this._horoscope3Days()
      }
    }
    
    if(prevProps.horoscope.refresh !== horoscope.refresh) {
      if(horoscope.list.length) {
        this._allUpdateHoroscopes()
      } else {
        this._horoscope3Days()
      }
      this.refreshSlider()
    }
  } 
  _allUpdateHoroscopes() {
    const { horoscope, actions } = this.props;
    let dates = this.dateList()
    this.setState({dates})
    if(horoscope.list.length) {
      let custom = horoscope.list.find((item) => item.name == "custom")
      if(custom) {
        dates.push(custom)
      }
      
      horoscope.list.map((item) => {
        console.log(item.name)
        actions.horoscope.addHoroscopeList(item.name, horoscope.zodiak, dates)
      })
    } 
  }
  dateList(d = Date.now()) {
    let {setting} = this.props
    //запрашиваем 12 дат, прошлого, будущего настоящего
    let dates = helper.datePastFuturePresent();
    let newDates = [];
    if(setting.calendar) {
      
      let day = setting.calendar.find((item) => item.period == "day")
      let month = setting.calendar.find((item) => item.period == "month")
      let week = setting.calendar.find((item) => item.period == "week")
      let year = setting.calendar.find((item) => item.period == "year")

      newDates = dates.filter((item) => {
          //отсеиваем даты не попадающие в диапазон установленный в настройках
          if(item.type == "day" & helper.isIncludedRangeDate(item.date, day.min_date, day.max_date)) {
            return item
          }
          if(item.type == "year" & helper.isIncludedRangeDate(item.date, year.min_date, year.max_date)) {
            return item
          }
          if(item.type == "week" & helper.isIncludedRangeDate(item.date, week.min_date, week.max_date)) {
            return item
          }
          if(item.type == "month" & helper.isIncludedRangeDate(item.date, month.min_date, month.max_date)) {
            return item
          }
      })
      
    }
    
    return (newDates.length > 0) ? newDates : dates;
  }
  isIncludedRangeDate (date, minDate, maxDate) {
    return new Date(minDate) <= new Date(date) && new Date(date) <= new Date(maxDate)
  }
  
  _horoscope3Days() {
      const { user, horoscope, actions } = this.props;
      let zodiak = horoscope.zodiak ? horoscope.zodiak : user.zodiak_sign
      console.log('zodiak', zodiak)
      
      
      actions.horoscope.addHoroscopeList("today", zodiak, this.state.dates)
      actions.horoscope.addHoroscopeList("yesterday", zodiak, this.state.dates)
      actions.horoscope.addHoroscopeList("tomorrow", zodiak, this.state.dates)

      //неделя, месяц, год загружаются для календаря
      actions.horoscope.addHoroscopeList("week", zodiak, this.state.dates)
      actions.horoscope.addHoroscopeList("month", zodiak, this.state.dates)
      actions.horoscope.addHoroscopeList("year", zodiak, this.state.dates)
  }
  
  
  _findHoroscope(type) {
    let {horoscope} = this.props
    let find = horoscope.list.find((item) => {
      return item.name == type
    })
    if(find) {
      return find
    }
    return null
  }
  _activeScreen() {
    let {slide_index} = this.state
    let item = this.state.dates[slide_index]
    
    return item;
    
  }
  _isRatePrev(current_screen, screen,  prev_screen) {
    //проверяет есть ли оценка за предыдущий гороскоп
    let prev_data = this._findHoroscope(prev_screen)
    if(current_screen == screen && prev_data) {
      if(prev_data.rate == null) {
        return true;
      }
    }
  }
  
  
  render() {
    let {actions, navigation, user, horoscope} = this.props
    let {slide_start_index, slide_index, end_slide_index} = this.state
    
    if(horoscope.list == 0 || slide_start_index == null) {
      return <Loading/>
    }
    let customHoroscopeItem = this._findHoroscope('custom');
    let activeScreen = customHoroscopeItem ? customHoroscopeItem : this._activeScreen();
    let isRateYesterday = this._isRatePrev(activeScreen.name, "today", "yesterday");
    let isHoroscopeCustom = false;
    if(navigation.state.params) {
      isHoroscopeCustom = navigation.state.params.type == "custom";
    }
    console.log('activeScreen', activeScreen.name)
    return (
      <View style={styles.container} >
          <StatusBar barStyle="light-content" />
          <Swiper 
              showsButtons={false}
              showsPagination={false}
              loop={false}
              index={slide_start_index}
              ref='swiper'
              onIndexChanged={(index) => this._onIndexChanged(index)}
              >
                {this.dateList().map((item, index) => {
                      return (
                        <HoroscopeItem 
                          key={index.toString()} 
                          navigation={navigation} 
                          type={item.type} 
                          data={this._findHoroscope(item.name)} 
                          actions={actions.horoscope}
                        />
                      )
                    })
                  }
            </Swiper> 
           {isHoroscopeCustom && customHoroscopeItem
            ? //Кастомный гороскоп поверх слайдера
            <View style={styles.absoluteBlock }>
                 <Swiper 
                    showsButtons={false}
                    showsPagination={false}
                    loop={false}
                    index={1}
                    onIndexChanged={(index) => actions.horoscope.delHoroscope('custom')}
                  >
                    {null}
                    <HoroscopeItem 
                      navigation={navigation} 
                      type={customHoroscopeItem.type} 
                      data={this._findHoroscope(customHoroscopeItem.name)} 
                      actions={actions.horoscope}
                      style={{backgroundColor: "#F2F2F3"}}
                    />
                    {null}
                </Swiper>
            </View>
         
            
            : null}
          <View style={[styles.footer]}>
             
              <View style={{flexDirection: "row", justifyContent: "space-between", height: 35}}>
               
                  <TouchableOpacity onPress={() => customHoroscopeItem ? actions.horoscope.delHoroscope('custom') : this.onSwipePrev() }>
                        <View>
                        <Feather name="chevron-left" size={34} color={ (slide_index == 0 ) ? Colors.grey : Colors.black} />
                        {isRateYesterday 
                          ? <LinearGradient
                              colors={["#E93C50","#FF906C"]}
                              style={styles.circle}
                            >
                          </LinearGradient> 
                          : null}
                        </View>
                    </TouchableOpacity>
                
                 

                {this._findHoroscope(activeScreen.name)
                  ? <IconShare message={this._shareText(activeScreen)} title={this._shareTitle(activeScreen)}/>
                  : null
                }        
                
                <TouchableOpacity onPress={() => customHoroscopeItem ? actions.horoscope.delHoroscope('custom') : this.onSwipeNext()}>
                    <Feather name="chevron-right" size={34} color={ (slide_index == end_slide_index) ? Colors.grey : Colors.black} />
                  </TouchableOpacity>
              
              </View>
          </View>
      </View>
    );
  }
  _parseText(text) {
    if(typeof text == 'object') {
      return text
    } else {
      return JSON.parse(text)
    }
  }
  getTopTitle() {
    let {data} = this.props
    let {locale} = this.state
    let date = new Date(data.horoscope.date)
    return helper.getTopTitleDate( data, date, locale )
  }
  getTitle() {
    let {locale} = this.state
    let {data} = this.props
    let date = data.horoscope.date
    return helper.getTitleDate( data, date, locale)
  }
  _shareTitle(item) {
    let { horoscope } = this._findHoroscope(item.name)
    if( horoscope ) {
      return `${TextZodiak({zodiak: horoscope.zodiak_sign})}. ${ helper.getTitleDate(item, horoscope.date, "ru") }. ${ helper.getTopTitleDate(item, horoscope.date, "ru") }`
    }
  }
  _shareText(item) {
    let {horoscope} = this._findHoroscope(item.name)
    if(horoscope) {
      return this._parseText(horoscope.texts).map((text) => {
        return text
      })
    }
  }
  _preloadItem(index) {
    //предзагрузка гороскопов
    let {dates} = this.state
    let {horoscope, actions} = this.props
    let prev_index = (index == 0) ? 0 : index - 1;
    let next_index = (index == dates.length -1) ? dates.length-1  :  index + 1;
    
    let prev =  this.state.dates[prev_index];
    let next =  this.state.dates[next_index];
    if(!this._findHoroscope(prev.name)) {
      actions.horoscope.addHoroscopeList(prev.name, horoscope.zodiak, dates)
    }
    if(!this._findHoroscope(next.name)) {
      actions.horoscope.addHoroscopeList(next.name, horoscope.zodiak, dates)
    }
  }
  _onIndexChanged(index) {
    console.log('index', index)
    this._preloadItem(index);
    this.setState({slide_index: index})
  }
  onSwipePrev() {
    swiper = this.refs.swiper
    let {slide_index} = this.state
    if (swiper) {
      let new_slide_index = slide_index - 1;
      console.log(new_slide_index)
      if(new_slide_index >= 0) {
        swiper.scrollBy(-1)
      }
    }
  }

  onSwipeNext() {
    swiper = this.refs.swiper
    let {slide_index, dates} = this.state
    if (swiper) {
      let new_slide_index = slide_index + 1;
      
      if(new_slide_index < dates.length) {
        swiper.scrollBy(1)
      }
    }
  }
}

export default connect(state => ({
  user: state.user,
  horoscope: state.horoscope,
  setting: state.setting
}),
(dispatch) => ({
  actions: {
    user: bindActionCreators(ActionsUser, dispatch),
    horoscope: bindActionCreators(ActionsHoroscope, dispatch),
    setting: bindActionCreators(ActionsSetting, dispatch)
  }
})
)(HoroscopeScreen);

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#F2F2F3",
    },  
    contentContainer: {
      alignContent: "center",
      justifyContent: 'center',
      alignItems: "center",
      flex: 1
    },
    absoluteBlock: {
      bottom: 0, 
      left: 0, 
      right:  0,
      position: "absolute", 
      top: 0, 
      flex: 1, 
      backgroundColor: "transparent"
    },
    footer: {
      padding: "4%",
      backgroundColor: "#fafafb"
    },
    shadow: {
      shadowOffset:{  width: 0,  height: -5,  },
      shadowColor: '#ececec',
      shadowOpacity: 0.5,
    },
    circle: {
      width: 14,
      height: 14,
      position: "absolute",
      right: 0,
      borderRadius: 8
    }
});
