import React from 'react';
import {
  Image,
  RefreshControl,
  ImageBackground,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';

import Markdown from 'react-native-markdown-renderer';

import IconZodiak from '../../../components/IconZodiak';
import TextZodiak from '../../../components/TextZodiak';
import Loading from '../../../components/Loading'

import Colors from '../../../constants/Colors';
import Locale from '../../../constants/Locale';
import helper from '../../../api/helper'

import ZodiakImages from '../../../constants/ZodiakImages';
import IconMenu from '../../../components/IconMenu';
import IconBack from '../../../components/IconBack';
import { Ionicons } from '@expo/vector-icons';
import RatingSlider from '../components/RatingSlider';

import i18n from '../../../i18n'

export default class HoroscopeItem extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      locale : i18n.t("locale"),
      isRefreshing: false,
    }
  }
  _parseText(text) {
    if(typeof text == 'object') {
      return text
    } else {
      return JSON.parse(text)
    }
  }
  renderRateDay(name, value) {
    let title, img = ""
    switch (name) {
      case "workRate":
        title =  i18n.t("work");
        img = require("../../../assets/images/sumka.png");
        break;
    
      case "loveRate":
        title =  i18n.t("love");
        img = require("../../../assets/images/heart.png");
      break;
        case "healthRate":
          title =  i18n.t("health");
          img = require("../../../assets/images/sun.png");
      break;
    }
    return (
      <View>
        <Text style={styles.rate_day_title}>{title}</Text>
        <View style={{flexDirection: "row", alignItems: "center", justifyContent: "space-between"}}>
          <Image source={ img } />
          <Text style={styles.rate_day_number}> {value}</Text>
        </View>
      </View>
    )
  }
  
  getTopTitle() {
    let {data} = this.props
    let {locale} = this.state
    let date = new Date(data.horoscope.date)
    return helper.getTopTitleDate( data, date, locale )
  }
  getTitle() {
    let {locale} = this.state
    let {data} = this.props
    let date = data.horoscope.date
    return helper.getTitleDate( data, date, locale)
  }
  _onRefresh() {
    let {actions, data} = this.props
    let isCustom = (data.name == "custom")
    if(!isCustom) {
      this.setState({isRefreshing: false});
      actions.refreshHoroscope()
    }
  }
  render() {
    let {actions, navigation, data, style} = this.props
    if(data == null) return <Loading/>
    let {name, horoscope} = data
    let isCustom = (data.name == "custom")
    let zodiak = horoscope.zodiak_sign
    return (
      <View style={[style, { flex: 1, }]}>
        <ScrollView contentContainer={styles.contentContainer} refreshControl={
            <RefreshControl
              refreshing={this.state.isRefreshing}
              onRefresh={()=>this._onRefresh()}
              // tintColor="#ff0000"
              // title="Loading..."
              // titleColor="#00ff00"
              // progressBackgroundColor="#ffff00"
            />
        }>
          <ImageBackground  
            source={ZodiakImages[ zodiak ]["block"]} 
            style={{width: '100%', paddingTop: 40}} 
            resizeMode="cover"
            >
                <View style={styles.header}>
                  <IconMenu color="white" navigation={navigation} style={styles.icon_menu}/>
                  <TouchableOpacity onPress={()=>navigation.navigate('Calendar')}>
                    <Image source={require('../../../assets/images/calendar_white.png')} />
                  </TouchableOpacity>
                </View>
                
                <View style={[styles.block_day, styles.padding_content]}>
                  <Text style={styles.weekday_title}>{this.getTopTitle()}</Text>
                  
                  <TouchableOpacity style={styles.day_title} 
                    onPress={()=>navigation.navigate('Calendar')} 
                  >
                    <Text style={styles.day_title_text} >{this.getTitle(name)} </Text>
                    <Ionicons
                        name={"ios-arrow-down"}
                        size={22}
                        style={{ paddingTop: 10 }}
                        color="white"
                      />
                  </TouchableOpacity>
                </View>

                <View style={[styles.padding_content, styles.shadow, {paddingBottom: 0}]}>
                  <View style={styles.zodiak_block}>

                    <IconZodiak zodiak={zodiak} type="shadow" style={styles.zodiak_icon}/>
                    
                    <TouchableOpacity 
                      onPress={()=>navigation.navigate('Zodiak')} 
                      >
                      <View style={styles.zodiak_title}>
                        <Text style={styles.zodiak_title_text}>
                          <TextZodiak zodiak={zodiak} />
                        </Text>
                        <Ionicons
                          name={"ios-arrow-down"}
                          size={22}
                          style={{ paddingTop: 15 }}
                          color={Colors.black}
                        />
                      </View>
                    </TouchableOpacity>
                  </View>
                  
                </View>
          </ImageBackground>
          <View style={[styles.padding_content, styles.shadow, {paddingTop: 0}]}>
            <View style={styles.rate_day}>
                {this.renderRateDay("workRate", horoscope.work_rate)}
                {this.renderRateDay("loveRate", horoscope.love_rate)}
                {this.renderRateDay("healthRate", horoscope.health_rate)}
            </View>
            {this._parseText(horoscope.texts).map((text, index) => {
              return  <View  key={index} style={[styles.description_block, styles.shadow]}>
                           <Markdown style={styles}>{text}</Markdown>
                      </View>
            })}
           
                
          </View>
        </ScrollView>
        <RatingSlider data={data} actions={actions} style={[styles.footer, styles.shadow, { }]}/> 
      </View>
    );
  }
}

const styles = StyleSheet.create({
    footer: {
      padding: "5%",
      paddingBottom: 0,
      backgroundColor: "#fafafb"
    },
    icon_menu: {
      left: 10
    },
    header: {
      flexDirection: "row",
      justifyContent: "space-between",
      paddingTop: 10, 
      paddingLeft: 10,
      paddingRight: 20
    },
    shadow: {
      shadowOffset:{  width: 0,  height: -5,  },
      shadowColor: '#ececec',
      shadowOpacity: 0.5,
    },
    contentContainer: {
      alignContent: "center",
      justifyContent: 'center',
      alignItems: "center",
      flex: 1,
      
    },
    shadow: {
      shadowOffset:{  width: 0,  height: 10,  },
      shadowColor: '#ececec',
      shadowOpacity: 0.5,
    },
    padding_content: {
      padding: "5%",
      
    },
    weekday_title: {
      fontSize: 14, 
      fontWeight: "bold",
      color: "white",
      opacity: 0.8,
      textTransform: "uppercase"
    },
    day_title: {
      flexDirection: "row",
      alignItems: "center",
      paddingRight: "4%"
    },
    day_title_text: {
      color: "white",
      fontWeight: "bold",
      fontSize: 36,
    },
    zodiak_block: {
      backgroundColor: "white", 
      justifyContent: 'flex-end', 
      height: 140, 
      borderRadius: 16
    },
    zodiak_title: {
      flexDirection: "row",
      justifyContent: "space-between",
      padding: 30,
      paddingBottom: 20
    },
    zodiak_title_text: {
      fontSize: 32,
      fontWeight: "bold",
      color: Colors.black
    },
    zodiak_icon: {
      position: "absolute", 
      top: -35, 
      left: -30
    },
    rate_day: {
      backgroundColor: "white", 
      borderRadius: 16,
      padding: 30,
      paddingBottom: 20,
      paddingTop: 20,
      flexDirection: "row",
      justifyContent: "space-between",
      marginTop: 20,
      marginBottom: 20,
      alignItems: "center"
    },
    rate_day_title: {
      textTransform: "uppercase",
      paddingBottom: 10,
      color: Colors.grey,
      fontSize: 10,
      textAlign: "center"
    },
    rate_day_number: {
      fontWeight: "bold",
      fontSize: 18,
      paddingLeft: 5
    },
    description_block: {
      backgroundColor: "white", 
      borderRadius: 16,
      padding: 30,
      paddingBottom: 20,
      paddingTop: 20,
      marginBottom: 20
    },
    heading: {
      color: Colors.grey,
      textTransform: "uppercase"
    },
    heading1: {
      fontSize: 18,
      backgroundColor: '#000000',
      color: '#FFFFFF',
    },
    heading2: {
      fontSize: 16,
    },
    heading3: {
      fontSize: 12,
    },
    heading4: {
      fontSize: 10,
    },
    heading5: {
      fontSize: 9,
    },
    heading6: {
      fontSize: 8,
    },
    text: {
      color: Colors.black,
      fontSize: 14,
      lineHeight: 23,
    }
});
