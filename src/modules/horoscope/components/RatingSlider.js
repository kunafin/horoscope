import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  ImageBackground
} from 'react-native';
import Slider from "react-native-slider";
import Colors from '../../../constants/Colors';
import helper from '../../../api/helper';
import i18n from '../../../i18n'

const TooltipValue = function({value}) {
  return (
    <View style={{alignItems: 'center', justifyContent: "center"}}>
      <ImageBackground 
        style={styles.tooltipImage} 
        source={require('../../../assets/images/tooltip2.png')}
        >
          <Text style={styles.tooltip_text}>
            {value}%
          </Text>
      </ImageBackground>
    </View>
  )
 
}

export default class RatingSlider extends React.Component {
  constructor(props) {
    super(props);
    this.state = { 
      defaultValue: 50,
      value: null,
      color: null,
      isShow: false,
      screens: [
        "year", "month", "week", "day_before_yesterday", "yesterday"
      ]
    };
  }
  componentDidMount() {
    let { data } = this.props;
    this._getRate(data)
  }
  componentDidUpdate(prevProps) {
    let { data } = this.props;
    if(prevProps.data !== data) {
      this._getRate(data)
    }
  }
  _getRate(data) {
   
    let {rate} = data
    let value = (rate) ? (rate.precision) : this.state.defaultValue;
    this.setState({value: value, color: this.getColorSlider(value)})
    
  }
  getColorSlider(value) {
    let color = "rgba(255, 59, 59, 1)"
    if(value > 50) {
      color = "rgba(255, 201, 0, 1)";
    }
    if(value > 75) {
      color = "rgba(0, 206, 21, 1)";
    }
    return color
  }
  _onValueChange(value) {
    
    this.setState({value, color: this.getColorSlider(value)})
  }
  _onSlidingComplete() {
    let {value} = this.state
    let { actions, data } = this.props
    actions.setRate(data.horoscope.id, value)
    this.setState({isShow: false})
  }
 
  render() {
    
    let {value, color, isShow} = this.state
    let {style, data, actions} = this.props
    console.log()
    return (
      <View>
        {helper.inArray(data.name, this.state.screens) ? (
          <View style={[styles.container,  style, {paddingTop: 20, paddingBottom: 0}]}>
         
          <Text style={styles.title}> 
            {i18n.t("evalute_forecast_prcision")} 
          </Text>
          
          <View style={{flexDirection: "row", justifyContent: "space-between", paddingLeft: "4%", paddingRight: "4%"}}>
  
            <View>{value == 0 && isShow ? <TooltipValue  value={value}/>: null}</View>
            <View>{value == 25 && isShow ? <TooltipValue  value={value}/>: null}</View>
            <View>{value == 50 && isShow ? <TooltipValue  value={value}/>: null}</View>
            <View>{value == 75 && isShow ? <TooltipValue  value={value}/>: null}</View>
            <View>{value == 100 && isShow ? <TooltipValue  value={value}/>: null}</View>
          </View>
               
          <View style={styles.stepAbsoluteBlock}>
            <View></View>
            <View style={ value != 25 ? styles.stepBorder : null }></View>
            <View style={ value != 50 ? styles.stepBorder : null }></View>
            <View style={ value != 75 ? styles.stepBorder : null }></View>
            <View></View>
          </View>

          <Slider
              style={{width: "100%", height: 28, borderRadius: 100,}}
              minimumValue={0}
              step={25}
              value={value}
              maximumValue={100}
              minimumTrackTintColor={color}
              onSlidingStart={()=> this.setState({isShow: true})}
              onSlidingComplete={()=> this._onSlidingComplete()}
              maximumTrackTintColor={"rgba(235, 235, 246, 0.8)"}
              trackStyle={{height: 6, borderRadius: 6}}
              thumbStyle={[{
                backgroundColor: "white",  
                height: 28,
                 width: 28, 
                 borderRadius: 20}, 
                 styles.shadow
                
                ]}
              
              onValueChange={(value) => this._onValueChange(value)}
          />
        </View>

        ) : null}
        
      </View>
      
    )
  } 
}

const styles = StyleSheet.create({
  container: {
    height: 75,

  },  
  stepAbsoluteBlock: {
    flexDirection: "row", 
    position: "absolute", 
    width: "100%", 
    top: 51,
    height: 6,
    borderRadius: 10,
    justifyContent: "space-between", 
    left: "5%", 
    right: "4%"
  },
  stepBorder: {
    borderRightWidth: 1, 
    borderRightColor: "rgba(0, 0, 0, 0.8)",
    height: 6
  },
  title: {
    color: Colors.grey,
    fontSize: 10,
    height: 20,
    textAlign: 'center',
    paddingBottom: 0
  },
  tooltip_text: {
    fontSize: 10
  },
  tooltipImage: {
    width: 100, 
    height: 90,  
    alignItems: "center", 
    paddingTop: 23,
    position: "absolute",
    top: -60
  },
  shadow: {
    shadowOffset:{  width: 1.5,  height: 2,  },
    shadowColor: '#ccc',
    shadowOpacity: 0.8,
  },
  
});
