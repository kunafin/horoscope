import React from 'react';
import {
    View,
    StyleSheet,
    Text,
    TouchableOpacity,
    FlatList
  } from 'react-native';


import ZodiakLang  from "../../../constants/ZodiakLang";
import TextZodiak from "../../../components/TextZodiak";
import IconZodiak from "../../../components/IconZodiak";

export default class ZodiakList extends React.Component {
    constructor(props) {
      super(props);
      this.state = { 
        items : ZodiakLang, 
        countLine: 3
      };
    }
    _onPressItem(item) {
        let {state, props} = this
        if(props.onPressItem) {
            props.onPressItem(item)
        }
    }
    render () {
        let {state, props} = this
        let {countLine} = state
        return (
            <View style={[props.style, {height: "100%"}]}>
                <FlatList
                    data={state.items}
                    keyExtractor={(item) => item.name}
                    numColumns={countLine}
                    renderItem={({item}) => 
                        <TouchableOpacity 
                            style={[styles.item, {width: 100 / countLine + "%"}]}
                            onPress={() => this._onPressItem(item.name)}
                        >
                            <IconZodiak style={styles.img} type="normal" zodiak={item.name} />
                            <Text style={styles.title}>
                                <TextZodiak zodiak={item.name}/>
                            </Text>
                        </TouchableOpacity>
                    }  
                />
            </View>
        )
    }
  }

  const styles = StyleSheet.create({
 
    
    item: {
        alignItems: "center",
        marginBottom: 10
    },
    img: {

    },
    title: {
        color: "white",
        textAlign: "center",
        paddingTop: 8
    }
  });