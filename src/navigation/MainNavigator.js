import React from 'react';
import { Platform , ScrollView, View, Text, Image} from 'react-native';
import { createStackNavigator, createDrawerNavigator, DrawerItems, SafeAreaView } from 'react-navigation';

import i18n from '../i18n';

import { connect } from 'react-redux';
import {bindActionCreators} from 'redux';
import * as ActionsHoroscope from '../actions/Horoscope';

import HoroscopeScreen from '../modules/horoscope/screens/HoroscopeScreen';
import ZodiakScreen from '../modules/horoscope/screens/ZodiakScreen';

import CalendarScreen from '../modules/calendar/screens/CalendarScreen';

import CompatibilityScreen from '../modules/compatibility/screens/CompatibilityScreen';
import ZodiakMyScreen from '../modules/compatibility/screens/ZodiakMyScreen';
import ZodiakPartnerScreen from '../modules/compatibility/screens/ZodiakPartnerScreen';

import SettingsScreen from '../modules/settings/screens/SettingsScreen';

import UsernameScreen from '../modules/personal/screens/UsernameScreen'
import DateBirthScreen from '../modules/personal/screens/DateBirthScreen'
import SexScreen from '../modules/personal/screens/SexScreen'

import FeedbackScreen from '../modules/feedback/screens/FeedbackScreen';
import FeedbackSuccessScreen from '../modules/feedback/screens/FeedbackSuccessScreen';

import AssessmentScreen from '../modules/assessment/screens/AssessmentScreen';

import HeaderMenu from '../components/HeaderMenu';
import Colors from '../constants/Colors';



const HoroscopeStack = createStackNavigator(
  {
    Horoscope: {
      screen: HoroscopeScreen,
      path: "Horoscope/:type"
    },
    Zodiak: ZodiakScreen,
    Calendar: CalendarScreen,
  }
);

HoroscopeStack.navigationOptions = () =>  {
  return {
    drawerLabel: i18n.t("horoscope"),
    drawerIcon: ({ focused }) => (
      <Image source={ focused ? 
          require('../assets/images/menu/calendar_active.png') : 
          require('../assets/images/menu/calendar.png')
      }/>
    ),
  }
  
};

const CompatibilityStack = createStackNavigator(
  {
    Compatibility: CompatibilityScreen,
    ZodiakMy: ZodiakMyScreen,
    ZodiakPartner: ZodiakPartnerScreen,
  }
);

CompatibilityStack.navigationOptions = () =>  {
  return {
    drawerLabel: i18n.t("compatibility"),
    drawerIcon: ({ focused }) => (
      <Image source={ focused ? 
        require('../assets/images/menu/heart_active.png') : 
        require('../assets/images/menu/heart.png')
      }/>
    ),
  }
};

const SettingsStack = createStackNavigator(
  {
    Settings: SettingsScreen,
    Username: UsernameScreen,
    DateBirth: DateBirthScreen,
    Sex: {
      screen: SexScreen,
      params: { navigate: 'Settings' }
    },
  }
);

SettingsStack.navigationOptions = () => {
  return {
    drawerLabel: i18n.t("setting"),
    drawerIcon: ({ focused }) => (
      <Image source={ focused ? 
        require('../assets/images/menu/setting_active.png') : 
        require('../assets/images/menu/setting.png')
      }/>
    ),
  }
};

const FeedbackStack = createStackNavigator(
  {
    Feedback: FeedbackScreen,
    FeedbackSuccess: FeedbackSuccessScreen
  }
);

FeedbackStack.navigationOptions = () => {
  return {
    drawerLabel: i18n.t("write_developer"),
    drawerIcon: ({ focused }) => (
      <Image source={ focused ? 
        require('../assets/images/menu/mail_active.png') : 
        require('../assets/images/menu/mail.png')
      }/>
    ),
  }
};

const AssessmentStack = createStackNavigator(
  {
    Assessment: AssessmentScreen,
  }
);

AssessmentStack.navigationOptions = () => {
  return {
    drawerLabel: i18n.t("rate_app"),
    drawerIcon: ({ focused }) => (
      <Image source={ focused ? 
        require('../assets/images/menu/star_active.png') : 
        require('../assets/images/menu/star.png')
      }/>
    ),
  }
};

const CustomDrawerContentComponent = props => (
  <ScrollView>
    <HeaderMenu/>
    <SafeAreaView style={{flex: 1, }} forceInset={{  horizontal: 'never' }}>
      <DrawerItems {...props} onItemPress = {(route) => {
          props.navigation.navigate(route.route.routeName)
          if(route.route.routeName == "HoroscopeStack") {
            //обновить экран гороскопов
            props.actions.horoscope.dropHoroscopeList()
            props.actions.horoscope.refreshHoroscope()
          }
          if(route.route.routeName == "CompatibilityStack") {
            props.navigation.navigate("ZodiakPartner")
          }
          props.navigation.closeDrawer()
      }
      }/>
    </SafeAreaView>
  </ScrollView>
);

const reduxCustomDrawerContentComponent =  connect(state => ({
}),
(dispatch) => ({
  actions: {
    horoscope: bindActionCreators(ActionsHoroscope, dispatch),
  }
})
)(CustomDrawerContentComponent);

const drawerNavigator = createDrawerNavigator({
  HoroscopeStack,
  CompatibilityStack,
  AssessmentStack,
  FeedbackStack,
  SettingsStack,
}, {
  contentComponent: reduxCustomDrawerContentComponent,
  contentOptions: {
    activeTintColor: Colors.theme,
    inactiveTintColor: "#4C4B5E",
    activeBackgroundColor: null,
  }
});



export default drawerNavigator;

