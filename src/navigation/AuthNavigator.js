import React from 'react';

import { createStackNavigator, createDrawerNavigator, DrawerItems, SafeAreaView } from 'react-navigation';


import UsernameScreen from '../modules/personal/screens/UsernameScreen'
import DateBirthScreen from '../modules/personal/screens/DateBirthScreen'
import SexScreen from '../modules/personal/screens/SexScreen'


import Colors from '../constants/Colors';


const AuthStack = createStackNavigator(
  {
    Username: UsernameScreen,
    Sex: SexScreen,
    DateBirth: DateBirthScreen
  }
);


export default AuthStack;

