export default [
    {
        name: "aries",
        lang: {
            ru: "Овен",
            en: "Aries"
        }
    },
    {
        name: "taurus",
        lang: {
            ru: "Телец",
            en: "Taurus"
        }
    },
    {
        name: "gemini",
        lang: {
            ru: "Близнецы",
            en: "Gemini"
        }
    },
    {
        name: "cancer",
        lang: {
            ru: "Рак",
            en: "Cancer"
        }
    },
    {
        name: "leo",
        lang: {
            ru: "Лев",
            en: "Leo"
        }
    },
    {
        name: "virgo",
        lang: {
            ru: "Дева",
            en: "Virgo"
        }
    },
    {
        name: "libra",
        lang: {
            ru: "Весы",
            en: "Libra"
        }
    },
    {
        name: "scorpio",
        lang: {
            ru: "Скорпион",
            en: "Scorpio"
        }
    },
    {
        name: "sagittarius",
        lang: {
            ru: "Стрелец",
            en: "Sagittarius"
        }
    },
    {
        name: "capricorn",
        lang: {
            ru: "Козерог",
            en: "Capricorn"
        }
    },
    {
        name: "aquarius",
        lang: {
            ru: "Водолей",
            en: "Aquarius"
        }
    },
    {
        name: "pisces",
        lang: {
            ru: "Рыбы",
            en: "Pisces"
        }
    }
];
