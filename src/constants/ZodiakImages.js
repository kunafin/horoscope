export default {
    aries : {
        normal: require("../assets/images/zodiak_list/aries.png"),
        shadow: require("../assets/images/zodiak_list_shadow/aries.png"),
        block: require("../assets/images/substrate/aries.png"),
    }, 
    taurus : {
        normal: require("../assets/images/zodiak_list/taurus.png"),
        shadow: require("../assets/images/zodiak_list_shadow/taurus.png"),
        block: require("../assets/images/substrate/taurus.png"),
    },
    gemini : {
        normal: require("../assets/images/zodiak_list/gemini.png"),
        shadow: require("../assets/images/zodiak_list_shadow/gemini.png"),
        block: require("../assets/images/substrate/gemini.png"),
    },
    cancer : {
        normal: require("../assets/images/zodiak_list/cancer.png"),
        shadow: require("../assets/images/zodiak_list_shadow/cancer.png"),
        block: require("../assets/images/substrate/cancer.png"),
    },
    leo : {
        normal: require("../assets/images/zodiak_list/leo.png"),
        shadow: require("../assets/images/zodiak_list_shadow/leo.png"),
        block: require("../assets/images/substrate/leo.png"),
    },
    virgo : {
        normal: require("../assets/images/zodiak_list/virgo.png"),
        shadow: require("../assets/images/zodiak_list_shadow/virgo.png"),
        block: require("../assets/images/substrate/virgo.png"),
    },
    libra : {
        normal: require("../assets/images/zodiak_list/libra.png"),
        shadow: require("../assets/images/zodiak_list_shadow/libra.png"),
        block: require("../assets/images/substrate/libra.png"),
    },
    scorpio : {
        normal: require("../assets/images/zodiak_list/scorpio.png"),
        shadow: require("../assets/images/zodiak_list_shadow/scorpio.png"),
        block: require("../assets/images/substrate/scorpio.png"),
    },
    sagittarius : {
        normal: require("../assets/images/zodiak_list/sagittarius.png"),
        shadow: require("../assets/images/zodiak_list_shadow/sagittarius.png"),
        block: require("../assets/images/substrate/sagittarius.png"),
    },
    capricorn : {
        normal: require("../assets/images/zodiak_list/capricorn.png"),
        shadow: require("../assets/images/zodiak_list_shadow/capricorn.png"),
        block: require("../assets/images/substrate/capricorn.png"),
    },
    aquarius : {
        normal: require("../assets/images/zodiak_list/aquarius.png"),
        shadow: require("../assets/images/zodiak_list_shadow/aquarius.png"),
        block: require("../assets/images/substrate/aquarius.png"),
    },
    pisces : {
        normal: require("../assets/images/zodiak_list/pisces.png"),
        shadow: require("../assets/images/zodiak_list_shadow/pisces.png"),
        block: require("../assets/images/substrate/pisces.png"),
    }
}