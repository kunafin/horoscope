const tintColor = '#6F4CFF';

export default {
  tintColor,
  theme: tintColor,
  tabIconDefault: '#ccc',
  tabIconSelected: tintColor,
  tabBar: '#fefefe',
  errorBackground: 'red',
  errorText: '#fff',
  warningBackground: '#EAEB5E',
  warningText: '#666804',
  noticeBackground: tintColor,
  noticeText: '#fff',
  black: "#4C4B5E",
  grey: "#ADADC0"
};
