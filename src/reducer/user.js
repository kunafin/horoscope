import * as types from '../actions/actionTypes';

const initialState = {};


export default function user(state = initialState, action = {}) {
  
  switch (action.type) {
    case types.USER_CREATE:
      return action.payload;

    case types.USER_UPDATE:
      return {
        ...state,
        ...action.payload
      };

    case types.USER_FETCH:
      return action.payload;

    case types.USER_DROP:
      return {};

    default:
      return state;
  }
}
