import * as types from '../actions/actionTypes';

const initialState = {calendar: null, locale: "ru"};


export default function setting(state = initialState, action = {}) {
  
  switch (action.type) {
    
    case types.SETTING_CALENDAR_FETCH:
      return {
          ...state,
          calendar: action.payload
      };
    case types.SETTING_LOCALE_FETCH:
      return {
          ...state,
          locale: action.payload
      };
      case types.SETTING_LOCALE_SET:
        return {
            ...state,
            locale: action.payload
        };    
    default:
      return state;
  }
}
