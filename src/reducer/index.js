import {combineReducers} from 'redux'


import user from './user'
import horoscope from './horoscope'
import calendar from './calendar'
import compatibility from './compatibility'
import setting from './setting'

export default combineReducers({
    user,
    horoscope,
    calendar,
    compatibility,
    setting
})