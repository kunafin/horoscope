import * as types from '../actions/actionTypes';

const initialState = {zodiak_my: null, zodiak_partner: "aries", compatibility: null};

export default function compatibility(state = initialState, action = {}) {
  
  switch (action.type) {
    case types.COMPATIBILITY_ZODIAK_MY_SET:
      return {
          ...state,
          zodiak_my: action.payload
      };

    case types.COMPATIBILITY_ZODIAK_PARTNER_SET:
        return {
            ...state,
            zodiak_partner: action.payload
        };
    case types.COMPATIBILITY_FETCH:
        return {
            ...state,
            compatibility: action.payload
        };
    default:
      return state;
  }
}
