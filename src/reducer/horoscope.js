import * as types from '../actions/actionTypes';

const replaceItemToList = (list, replaceItem, name_type = "custom") => {
  if( list.find((item) => item.name == name_type) ) {
    list = list.map((item) => {
        if(item.name == name_type) {
          item = replaceItem
        }
        return item;
    })
  } else {
    list.push(replaceItem);
  }
  return list;
}
const delItemList = (list, name_type) => {
  return list.filter((item) => item.name != name_type)
}
const initialState = {list : [], zodiak: null, rate: [], refresh: false};


export default function horoscope(state = initialState, action = {}) {
  
  switch (action.type) {
    
    case types.HOROSCOPE_FETCH:
      return action.payload;
    case types.HOROSCOPE_LIST_ADD: 
    
      return {
        ...state,
        list: replaceItemToList(state.list, action.payload, action.payload.name)
      }
    case types.HOROSCOPE_CUSTOM_ADD: 

      //console.log('getHoroscopeList', list)
      return {
        ...state,
        list: replaceItemToList(state.list, action.payload, "custom")
      }
    case types.HOROSCOPE_DELETE: 

      //console.log('getHoroscopeList', list)
      return {
        ...state,
        list: delItemList(state.list, action.payload)
      }  
    case types.HOROSCOPE_LIST_DROP:
      return {
        ...state,
        list: []
      };
    case types.HOROSCOPE_REFRESH: {
      
      return {
        ...state,
        //list: [],
        refresh: state.refresh ? false : true
      };
    }
    case types.HOROSCOPE_RATE_ADD: 
      console.log('HOROSCOPE_RATE_ADD', action.payload.rate.rate)
      let new_list = state.list.map((item) => {
        if(item.horoscope.id == action.payload.horoscope_id) {
          item.rate = action.payload.rate.rate
        }
       
        return item;
      })
      
      return {
        ...state,
        list: new_list
      }
      case types.HOROSCOPE_ZODIAK_SET:
        return {
          ...state,
          zodiak: action.payload
        };
    case types.HOROSCOPE_DROP:
      return {};
    
    default:
      return state;
  }
}
