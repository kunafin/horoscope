import * as types from '../actions/actionTypes';

const initialState = {date: null, type: null};


export default function calendar(state = initialState, action = {}) {
  
  switch (action.type) {
    
    case types.CALENDAR_SET:
      return action.payload;
    
    default:
      return state;
  }
}
