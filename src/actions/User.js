
import * as types from './actionTypes';
import * as api from '../api'
import Token from '../api/token'
const User = {
    getUser(){
        return (dispatch, getState, {}) => {
            try {
                return api.getUser().then((res) => {
                    let user = res.user;
                    if(user) {
                        dispatch({
                            type: types.USER_FETCH, 
                            payload: user
                        })
                    } else {
                        this.deleteUser()
                    }
                    
                })
            } catch(err) {
                console.log(err)
            }
        }
    },
    setUser(user) {
        return (dispatch, getState, {}) => {
            try {
                api.createOrUpdateUser({
                    name: user.name,
                    date_of_birth: `${user.year}-${user.month}-${user.day}`,
                    gender: user.gender
                }).then((res) => {
                    let user = res.user;
                    Token.setToken(user.token)
                    console.log('setUser', user)
                    dispatch({
                        type: types.USER_CREATE, 
                        payload: user
                    })
                })
            } catch (error) {
                console.log(error)
            }
            
        }
    },
    setUserFields(fields) {
        return (dispatch, getState, {}) => {
            dispatch({
                type: types.USER_UPDATE, 
                payload: fields
            })
        }
    },
    deleteUser() {
        return (dispatch, getState, {}) => {
            Token.delToken()
            dispatch({
                type: types.USER_DROP
            })
        }
    },
}

module.exports = User;