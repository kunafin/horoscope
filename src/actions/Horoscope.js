
import * as types from './actionTypes';
import * as api from '../api'
import Token from '../api/token'
const Horoscope = {
    
    setZodiak(zodiak) {
        return (dispatch, getState, {}) => {
            dispatch({
                type: types.HOROSCOPE_ZODIAK_SET, 
                payload: zodiak
            })
        }
    },
    getCustomeHoroscope(period, date, zodiak_sign) {
        return (dispatch, getState, {}) => {
            try {
                return api.getHoroscope(period, date, zodiak_sign).then((horoscope) => {
                    
                    if((!horoscope.error)) {
                        
                        let id = horoscope.horoscope.id
                        api.getRate(id).then((rate) => {
                            dispatch({
                                type: types.HOROSCOPE_CUSTOM_ADD, 
                                payload: {
                                    "date": date,
                                    "name": "custom",
                                    "type": period, 
                                    ...horoscope, 
                                    rate: (rate.error) ? null : rate.rate
                                }
                            })
                        })
                    } 
                })
            } catch(err) {
                console.log(err)
            }
        }
    },
    delHoroscope(type="custom") {
        return (dispatch, getState, {}) => {
            dispatch({
                type: types.HOROSCOPE_DELETE, 
                payload: type
            })
        }
    },
    addHoroscopeList(name, zodiak, dates) {
        return (dispatch, getState, {}) => {
            try {
                let horoscope_date = dates.find((item, index) => {
                    return item.name == name
                })
                if(horoscope_date) {
                    api.getHoroscope(horoscope_date.type, horoscope_date.date, zodiak).then((horoscope) => {
                        if((!horoscope.error)) {
                            let id = horoscope.horoscope.id
                            api.getRate(id).then((rate) => {
                                dispatch({
                                    type: types.HOROSCOPE_LIST_ADD, 
                                    payload: {
                                        ...horoscope_date, 
                                        ...horoscope, 
                                        rate: (rate.error) ? null : rate.rate
                                    }
                                })
                            })
                        } 
                    });
                }
               
            } catch(err) {
                console.log(err)
            }
        }
    },
    getHoroscopeList(zodiak, dates) {
        return (dispatch, getState, {}) => {
            try {
                dispatch({
                    type: types.HOROSCOPE_LIST_DROP, 
                })
                dates.map((item, index) => {
                    api.getHoroscope(item.type, item.date, zodiak).then((horoscope) => {

                        
                        if((!horoscope.error)) {
                            let id = horoscope.horoscope.id
                            api.getRate(id).then((rate) => {
                                dispatch({
                                    type: types.HOROSCOPE_LIST_ADD, 
                                    payload: {
                                        ...item, 
                                        ...horoscope, 
                                        rate: (rate.error) ? null : rate.rate
                                    }
                                })
                            })
                        } 
                    })
                })
            } catch(err) {
                console.log(err)
            }
        }
    },
    refreshHoroscope() {
        return (dispatch, getState, {}) => {
            try {
                dispatch({
                    type: types.HOROSCOPE_REFRESH, 
                })
            } catch(err) {
                console.log(err)
            }
        }
    },
    dropHoroscopeList() {
        return (dispatch, getState, {}) => {
            try {
                dispatch({
                    type: types.HOROSCOPE_LIST_DROP, 
                })
            } catch(err) {
                console.log(err)
            }
        }
    },
    setRate(horoscope_id, rate) {
        return (dispatch, getState, {}) => {
            try {
                api.setRate(horoscope_id, rate).then((res) => {
                    console.log(' api.setRate', horoscope_id, rate, res)
                    dispatch({
                        type: types.HOROSCOPE_RATE_ADD, 
                        payload: {
                            horoscope_id, 
                            rate: res
                        }
                    })
                })
            } catch(err) {
                console.log(err)
            }
        }
    }
   
}

module.exports = Horoscope;