
import * as types from './actionTypes';
import * as api from '../api'
import i18n from '../i18n';

import * as Localization from 'expo-localization';
const Setting = {
    
    getSettingCalendar() {
        return (dispatch, getState, {}) => {
            try {
                api.getSettingCalendar().then((res)=>{
                    console.log('getSettingCalendar', res)
                    dispatch({
                        type: types.SETTING_CALENDAR_FETCH, 
                        payload: res.calendar
                    })
                })
                
            } catch(err) {
                console.log(err)
            }
        }
    },
    getLocale() {
        return (dispatch, getState, {}) => {
            try {
               return api.getLocale().then((locale)=>{
                    console.log('getLocale', locale)
                    if(locale) {
                        dispatch({
                            type: types.SETTING_LOCALE_FETCH, 
                            payload: locale
                        })
                    } else {
                        this.setLocale(Localization.locale)
                    }
                    return locale;
                })
                
            } catch(err) {
                console.log(err)
            }
        }
    },
    setLocale(locale) {
        return (dispatch, getState, {}) => {
            try {
                locale = locale.split('-')[0]
                api.setLocale(locale).then((res)=>{
                    console.log('setLocale', res)
                    i18n.locale = res
                    dispatch({
                        type: types.SETTING_LOCALE_SET, 
                        payload: res
                    })
                })
                
            } catch(err) {
                console.log(err)
            }
        }
    }
 }

module.exports = Setting;