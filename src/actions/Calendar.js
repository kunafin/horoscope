
import * as types from './actionTypes';
import * as api from '../api'
const Calendar = {
    
    setCalendar(type, date) {
        return (dispatch, getState, {}) => {
            try {
                
                dispatch({
                    type: types.CALENDAR_SET, 
                    payload: {
                        type,
                        date
                    }
                })
            } catch(err) {
                console.log(err)
            }
        }
    },
    
}

module.exports = Calendar;