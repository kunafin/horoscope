
import * as types from './actionTypes';
import * as api from '../api'
const Compatibility = {
    getCompatibility(zodiak_my, zodiak_partner) {
        return (dispatch, getState, {}) => {
            try {
                api.getCompatibility(zodiak_my, zodiak_partner).then((res) => {
                    //console.log(' api.getCompatibility',res)
                    dispatch({
                        type: types.COMPATIBILITY_FETCH, 
                        payload: res.compatibility
                    })
                })
            } catch(err) {
                console.log(err)
            }
        }
    },
    setZodiakMy(zodiak) {
        return (dispatch, getState, {}) => {
            try {
                dispatch({
                    type: types.COMPATIBILITY_ZODIAK_MY_SET, 
                    payload: zodiak
                })
            } catch(err) {
                console.log(err)
            }
        }
    },
    setZodiakPartner(zodiak) {
        return (dispatch, getState, {}) => {
            try {
                dispatch({
                    type: types.COMPATIBILITY_ZODIAK_PARTNER_SET, 
                    payload: zodiak
                })
            } catch(err) {
                console.log(err)
            }
        }
    },
    
}

module.exports = Compatibility;