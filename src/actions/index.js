import * as types from './actionTypes';
import {dateFormat} from '../api'


export const getErrors = () => {
    return (dispatch, getState, {errorDao}) => {
        let errors = errorDao.getAll()
        if (errors) {
            let res = errors.slice(0, errors.length); //copying array as realmdb returns a proxy
            return res
        } 
    }
}