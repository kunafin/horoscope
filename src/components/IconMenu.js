import React from 'react';
import {
  Image,
  TouchableOpacity,
} from 'react-native';
import { DrawerActions } from 'react-navigation-drawer';

export default function IconMenu({navigation, style, color}) {
  return (
    <TouchableOpacity 
      style={[{top: 0}, style ]} 
      onPress={()=>navigation.dispatch(DrawerActions.toggleDrawer())}
    >
        <Image source={(color == "black") ? require("../assets/images/menu/menu_black.png") : require("../assets/images/menu/menu.png")} />
    </TouchableOpacity>
  );
}
