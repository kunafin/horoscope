import React from 'react';
import {
  Image,
  TouchableOpacity,
  Share,
} from 'react-native';
import { Ionicons ,  Feather} from '@expo/vector-icons';
import Colors from '../constants/Colors'
import config from '../../app.json'

import i18n from '../i18n';

export default function IconShare({style, title, message, color = Colors.black}) {
  onShare = async () => {
    try {
      let text = message.slice();
      text = text.map((txt) => txt.replace(/#/g, ''))
      text.unshift(title)
      text.push(i18n.t("download_app") + ":")
      text.push(config.link_appstore)
      text.push(config.link_playmarket)

      const result = await Share.share({
        message: text.join("\n"),
        title: title,
      });

      if (result.action === Share.sharedAction) {
        if (result.activityType) {
          // shared with activity type of result.activityType
        } else {
          // shared
        }
      } else if (result.action === Share.dismissedAction) {
        // dismissed
      }
    } catch (error) {
      alert(error.message);
    }
  };
  return (
    <TouchableOpacity onPress={this.onShare} style={style} >
        <Ionicons name="md-share" size={32} color={color} />
    </TouchableOpacity>
  );
}
