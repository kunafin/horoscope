import React from 'react';
import {
  StyleSheet,
  Text,
  View,
} from 'react-native';
import IconBack from "../components/IconBack";

export default function Header({title, color, style, navigation}) {
    return (
      <View style={[styles.header, style]}>
        <IconBack navigation={navigation} color={color ? color : "black"}/>
        <Text style={[styles.title, {color: color ? color : "black"}]}>{title}</Text>
        <View></View>
      </View>
    )
}

const styles = StyleSheet.create({
    header: {
        flexDirection: "row", 
        justifyContent: "space-between", 
    },
    title: {
      color: "white",
      fontSize: 24,
      fontWeight: "bold",
      textAlign: "center",
      paddingBottom: 30,
  },
  });