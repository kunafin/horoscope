import React from 'react';
import {
    Image,
    View,
  } from 'react-native';

import ZodiakImages from '../constants/ZodiakImages';

export default function IconZodiak({style, zodiak, type = "normal"}) {
  return (
    <Image style={[style]} source={ZodiakImages[zodiak][type]}  />
  );
}
