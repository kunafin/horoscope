import React from 'react';
import {
  Image,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';

import { connect } from 'react-redux';
import {bindActionCreators} from 'redux';
import * as ActionsUser from '../actions/User';
import IconZodiak from '../components/IconZodiak';
import TextZodiak from '../components/TextZodiak';
import helper from '../api/helper'

function HeaderMenu({navigation, user}) {
  if(!user) return null
  let {name, zodiak_sign, date_of_birth} = user
 
  if(name && zodiak_sign && date_of_birth) {
    return (
      <View style={styles.container}>
          <View style={styles.containerContent}>
  
              <IconZodiak type="normal" zodiak={zodiak_sign} style={styles.img}/>
  
              <View style={{justifyContent: "center"}}>
  
                  <Text style={styles.name}>{name} </Text>
  
                  <Text style={styles.description}>
                      {zodiak_sign ? <Text><TextZodiak zodiak={zodiak_sign} lang="ru"/></Text> : null} • {helper.formatDate(date_of_birth, "dd.mm.yyyy")}
                  </Text>
  
              </View>
          </View>
        <View style={styles.hr}></View>
      </View>
    );
  } else {
    return null
  }

}
export default connect(state => ({
    user: state.user,
  }),
  (dispatch) => ({
    actions: {
      user: bindActionCreators(ActionsUser, dispatch),
    }
  })
  )(HeaderMenu);

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 15,
        paddingTop: 70,
        paddingBottom: 0,
        
    },  
    containerContent: {
        flexDirection: "row",
        alignContent: "center",
        alignItems: "stretch",
        justifyContent: 'center',
        paddingRight: 40,
        paddingLeft: 40
    },
    img: {
        marginRight: 20
    },
    name: {
        fontSize: 24,
        fontWeight: "bold"
    },
    description: {
        fontSize: 16,
        color: "#ADADC0",
        paddingTop: 5
    },
    hr: {
        paddingTop: 40,
        borderBottomColor: "#E7E7E7",
        borderBottomWidth: 1,
        width: "100%"
    }
});
