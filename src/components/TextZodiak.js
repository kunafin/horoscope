import React from 'react';
import {
    Text,
  } from 'react-native';

import i18n from '../i18n';

import ZodiakLang from '../constants/ZodiakLang'
// {ZodiakLang[props.zodiak][props.lang]}
export default function TextZodiak({zodiak, style}) {
  let text = ""
  
  ZodiakLang.map((item)=>{
    if(item.name == zodiak) {
      text = item.lang[ i18n.t("locale") ]
    }
  })
  return text
}
